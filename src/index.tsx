import 'bootstrap/dist/css/bootstrap.min.css';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import 'react-hot-loader';
import Notifications from 'react-notify-toast';
import { Router } from 'react-router-dom';
import App from './app/App';
import { AppContextProvider } from './app/context/AppContext';
import { AuthContextProvider } from './app/context/AuthContext';
import history from './app/utils/history';
// * Bootstraps the application.
ReactDOM.render(
  <React.StrictMode>
    <AuthContextProvider>
      <AppContextProvider>
        <Router history={history}>
          <Notifications />
          <App />
        </Router>
      </AppContextProvider>
    </AuthContextProvider>
  </React.StrictMode>,
  document.getElementById('root'),
);
