import './index.scss';
import history from '../../history';
import * as React from 'react';

export const Header = () => {
  return (
    <div className="header text-center">
      <div className="logo">
        <a onClick={() => history.push('/')}>
          <img src={`${process.env.PUBLIC_URL}/assets/img/logo.png`} />
        </a>
        <p>
          T3.6 Identification of cross-regional value-chains <br /> through open innovation
        </p>
      </div>
    </div>
  );
};
