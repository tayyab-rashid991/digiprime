import './index.scss';
import React from 'react';

interface Props {
  // Header
  index: number;
  title: string;
  companyCount: number;
  // Body
  gathering: number;
  primaryMaterial: number;
  production: number;
  packing: number;
  useService: number;
  collection: number;
  disposal: number;
  maintenance: number;
  repair: number;
  reuse: number;
  remanufacturing: number;
  recyclingLoopOpen: number;
  recyclingLoopClose: number;
  biochemicalRecovery: number;
}

const DashboardIndustry: React.FC<Props> = ({
  index,
  title,
  companyCount,
  gathering,
  primaryMaterial,
  production,
  packing,
  useService,
  collection,
  disposal,
  maintenance,
  repair,
  reuse,
  remanufacturing,
  recyclingLoopOpen,
  recyclingLoopClose,
  biochemicalRecovery,
}) => {
  const chainBlock = (value, numAction) => {
    const disable = numAction === 0 && 'block_disable';
    return (
      <div className={`table_body_col block_action ${disable}`}>
        <span>{value}</span>
        {numAction > 0 &&
          <div className="number number_transform1">
            {numAction}
          </div>
        }
      </div>
    );
  };

  const actionBlock = (
    value,
    spanSize,
    numAction,
    lineBorder?,
    leftArrow?,
    rightArrow?,
  ) => {
    const disable = numAction === 0 ? 'block_disable' : 'block_active';
    return (
      <div className={`table_body_col ${spanSize} ${disable}`}>
        <span>{value}</span>
         {numAction > 0 &&
            <div className="number number_transform2">
              {numAction}
            </div>
          }
          { lineBorder === true &&
            <React.Fragment>
              <div className="left_line"></div>
              <div className="right_line"></div>
            </React.Fragment>
          }
          { leftArrow === true && <span className="left_arrow">&#x2191;</span> }
          { rightArrow === true && <span className="right_arrow">&#x2193;</span> }
      </div>
    );
  };

  const blankBlock = (spanSize) => {
    return (
      <div className={`table_body_col ${spanSize}`}
        style={{
          borderLeft: '2px solid #439539',
          marginTop: '-10px',
          transform: 'translateX(-5px)',
        }}>
        <span style={{position: 'absolute', left: '-6px', top: '-5px', color: '#439539'}}>&#x2191;</span>
    </div>
    );
  };

  return (
    <div className="wrapper">
      <div className="table_header">
        <div className="table_header_col">
          <span>{index}</span>
          <span>{title}</span>
        </div>
        <div className="table_header_col">
          <span>Companies involved: {companyCount}</span>
          <span>Type: Regional</span>
        </div>
      </div>
      <div className="table_body">
        {chainBlock('Gathering of Core Resources', gathering)}
        {chainBlock('Primary Material Processing', primaryMaterial)}
        {chainBlock('Production', production)}
        {chainBlock('Packaging & Distribution', packing)}
        {chainBlock('Use/Service', useService)}
        {chainBlock('Collection', collection)}
        {chainBlock('Disposal', disposal)}
        {blankBlock('rowStart5')}
        {blankBlock('rowStart4')}
        {blankBlock('rowStart3')}
        {blankBlock('rowStart1')}
        {actionBlock('Maintenance', 'span1', maintenance, true, true, true)}
        <div className="table_body_col rowStart1">
          <span className="right_line"></span>
        </div>
        <div className="table_body_col rowStart4"></div>
        {actionBlock('Repair', 'span3', repair, true)}
        {actionBlock('Reuse/Refurbish', 'span3', reuse, true)}
        {actionBlock('Remanufactuing', 'span4', remanufacturing, true)}
        {actionBlock('Recycling (Close Loop)', 'span5', recyclingLoopOpen, true)}
        {actionBlock('Recycling (Open Loop)', 'span1', recyclingLoopClose)}
        {actionBlock('Biochemical feedstock recovery', 'span6', biochemicalRecovery, true, false, true)}
      </div>
      <div className="table_footer">
        <div className="table_footer_btn">
          More Details
        </div>
      </div>
    </div>
  );
};

export default DashboardIndustry;