import {
  faCertificate,
  faChartPie,
  faImage,
  faLightbulb,
  faLink,
  faPaperPlane,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import history from 'app/utils/history';
import classNames from 'classnames';
import React from 'react';
import { useLocation } from 'react-router';
import { Link } from 'react-router-dom';
import { Nav, NavItem, NavLink } from 'reactstrap';
import './index.scss';

const SideBar = () => {
  const location = useLocation().pathname;
  return (
    <div className={classNames('sidebar')}>
      <div className="side-menu">
        <Nav vertical className="list-unstyled pb-3">
          <div className="logo">
            <a onClick={() => history.push('/')}>
              <img src={`${process.env.PUBLIC_URL}/assets/img/logo.png`} />
            </a>
            <p>T3.9 - Barriers identification and legislation support</p>
          </div>
        </Nav>
        <Nav vertical className="list-unstyled pb-3">
          <NavItem>
            <NavLink className={location} tag={Link} to={'/about'}>
              <FontAwesomeIcon icon={faLink} className="mr-2" />
              Value Chains
            </NavLink>
          </NavItem>
          {/* <SubMenu title="Home" icon={faHome} items={submenus[0]} /> */}
          <NavItem>
            <NavLink tag={Link} to={'/pages'}>
              <FontAwesomeIcon icon={faCertificate} className="mr-2" />
              Synergies
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink tag={Link} to={'/pages'}>
              <FontAwesomeIcon icon={faImage} className="mr-2" />
              Hotspots
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              tag={Link}
              className={location === '/dashboard/smart-specialization' ? 'active' : ''}
              to={'/dashboard/smart-specialization'}
            >
              <FontAwesomeIcon icon={faLightbulb} className="mr-2" />
              Smart Specialization
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink tag={Link} to={'/pages'}>
              <FontAwesomeIcon icon={faCertificate} className="mr-2" />
              Swot Analysis
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              tag={Link}
              className={location === '/dashboard/focus-sector' ? 'active' : ''}
              to={'/dashboard/focus-sector'}
            >
              <FontAwesomeIcon icon={faChartPie} className="mr-2" />
              Focus Sectors
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              tag={Link}
              className={location === '/dashboard/companies' ? 'active' : ''}
              to={'/dashboard/companies'}
            >
              <FontAwesomeIcon icon={faChartPie} className="mr-2" />
              Companies
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink tag={Link} to={'/contact'}>
              <FontAwesomeIcon icon={faPaperPlane} className="mr-2" />
              Contact
            </NavLink>
          </NavItem>
        </Nav>
      </div>
    </div>
  );
};

export default SideBar;
