import * as React from 'react';
import { Multiselect } from 'multiselect-react-dropdown';
import { API_BASE_URL } from 'app/config';

interface IProps {
  onSelect?: Function;
}

export const NaceCodeSelect: React.FC<IProps> = ({ onSelect }) => {
  const [naceCodes, setNaceCodes] = React.useState([]);
  const getNaceCodes = async () => {
    const getNaceCodesData = await fetch(`${API_BASE_URL}naceCodes`, {
      method: 'GET',
    });
    if (getNaceCodesData.status === 200) {
      const jsonData = await getNaceCodesData.json();
      setNaceCodes(
        jsonData['_embedded']['naceCodes'].map((c) => ({
          ...c,
          value: `${c.naceCodeCode} (${c.naceCodeDescr})`,
        })),
      );
    }
  };
  React.useEffect(() => {
    getNaceCodes();
  }, []);

  return (
    <Multiselect
      options={naceCodes}
      displayValue="value"
      showCheckbox={true}
      onSelect={onSelect as any}
      selectionLimit="3"
    />
  );
};
