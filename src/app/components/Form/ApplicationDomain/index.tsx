import * as React from 'react';
import { Multiselect } from 'multiselect-react-dropdown';
import { API_BASE_URL } from 'app/config';

interface IProps {
  onSelect?: Function;
}

export const ApplicationDomainSelect: React.FC<IProps> = ({ onSelect }) => {
  const [applicationDomain, setApplicationDomain] = React.useState([]);
  const getApplicationDomain = async () => {
    const getDomainData = await fetch(`${API_BASE_URL}applicationDomains`, {
      method: 'GET',
    });
    if (getDomainData.status === 200) {
      const jsonData = await getDomainData.json();
      setApplicationDomain(jsonData['_embedded']['applicationDomains']);
    }
  };
  React.useEffect(() => {
    getApplicationDomain();
  }, []);

  return (
    <Multiselect
      options={applicationDomain}
      displayValue="applicationDomainDescr"
      showCheckbox={true}
      onSelect={onSelect as any}
    />
  );
};
