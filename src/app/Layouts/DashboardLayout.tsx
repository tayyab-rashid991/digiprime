/** @jsxImportSource @emotion/react */
import { DashboardHeader } from 'app/components/DashboardHeader';
import Sidebar from 'app/components/DashboardSidebar';
import * as React from 'react';
import { Route, withRouter } from 'react-router-dom';

interface IProps {
  exact?: boolean;
  path: string;
  component: React.ComponentType<any>;
}

const DashboardLayout = ({ component: Component, ...otherProps }: IProps) => {
  return (
    <div className="main">
      <Route
        render={(otherProps) => (
          <React.Fragment>
            <DashboardHeader />
            <Sidebar />
            <div css={{ paddingLeft: '255px' }}>
              <Component {...otherProps} />
            </div>
          </React.Fragment>
        )}
      />
    </div>
  );
};

export default withRouter(DashboardLayout as any) as any;
