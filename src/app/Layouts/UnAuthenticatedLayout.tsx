import * as React from 'react';
import { Route, withRouter } from 'react-router-dom';

interface IProps {
  exact?: boolean;
  path: string;
  component: React.ComponentType<any>;
}

const UnauthenticatedLayout = ({ component: Component, ...otherProps }: IProps) => {
  return (
    <React.Fragment>
      <div className="main">
        <Route
          render={(otherProps) => (
            <React.Fragment>
              <Component {...otherProps} />
            </React.Fragment>
          )}
        />
      </div>
    </React.Fragment>
  );
};

export default withRouter(UnauthenticatedLayout as any) as any;
