/** @jsxImportSource @emotion/react */
import { css } from '@emotion/react';
import history from 'app/history';
import { AvField, AvForm } from 'availity-reactstrap-validation';
import { ApplicationDomainSelect } from 'app/components/Form/ApplicationDomain';
import { Button } from 'app/components/Form/Button';
import { Header } from 'app/components/Header';
import { InnovationContext, InnovationContextProvider } from 'app/context/InnovationContext';
import * as React from 'react';
import { Col, Container, FormGroup, Label } from 'reactstrap';

const AddInnovationPage = () => {
  const { addInnovation } = React.useContext<any>(InnovationContext);
  const [inputs, setInputs] = React.useState<any>({});
  const handleInputChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
    const el = e.target as HTMLInputElement;
    setInputs((inputs) => ({ ...inputs, [el.name]: el.value }));
  };
  const onSelect = (selectedList, selectedItem, name) => {
    delete selectedItem._links;
    setInputs((inputs) => ({ ...inputs, [name]: selectedItem }));
  };
  const handleSubmit = async () => {
    console.log(inputs);
    const success = await addInnovation(inputs);
    if (success) {
      history.push('innovation-overview');
    }
  };
  return (
    <React.Fragment>
      <Header />
      <Container>
        <div className="title text-center">
          <h2>Add Your Innovation Capabilities</h2>
        </div>
        <AvForm onSubmit={handleSubmit}>
          <div
            className="add-company-form"
            css={css`
              .detail-field {
                border: 1px solid #e5e5e5;
                border-radius: 5px;
                padding: 5px;
                font-size: 11px;
                color: #000;
                min-height: 55px;
                margin-bottom: 10px;
                position: relative;
                p {
                  margin: 0;
                }
              }
              svg {
                position: absolute;
                font-size: 12px;
                right: -20px;
                top: 35%;
                cursor: pointer;
              }
            `}
          >
            <FormGroup row>
              <Label sm={4}>Name of existing facility</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="facilityName"
                  id="facilityName"
                  placeholder="Pilot Plants"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={inputs.facilityName}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter facility name',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>
                Position in the secondary flow of the circular value chain of the existing demo
                centre/pilot line
              </Label>
              <Col sm={8}>
                <ApplicationDomainSelect
                  onSelect={(selectedList, selectedItem) =>
                    onSelect(selectedList, selectedItem, 'applicationDomain')
                  }
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>List of enabling and available technology & Machinery</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="machinery"
                  id="machinery"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={inputs.machinery}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter machinery',
                    },
                  }}
                />
                {/* <div className="action-field">
                 <div className="detail-field">
                   <p>
                     this is the description
                   </p>
                   <FontAwesomeIcon icon={faMinusCircle} onClick={() => console.log('clicked')}/>
                 </div>
                 <div className="action">
                   <input
                     name="strength"
                     placeholder="Add more"
                     onChange={e => console.log(e)}
                     onKeyDown={e => console.log(e)}
                   />
                   <FontAwesomeIcon icon={faPlusCircle} onClick={() => console.log('this')}/>
                 </div>
               </div> */}
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Owner</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="owner"
                  id="owner"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={inputs.owner}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter owner',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Type of access</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="access"
                  id="access"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={inputs.access}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter type of access',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Supported activities</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="supported"
                  id="supported"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={inputs.supported}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter supported activities',
                    },
                  }}
                />
                {/* <div className="action-field">
               <div className="detail-field">
                 <p>
                   this is the description
                 </p>
                 <FontAwesomeIcon icon={faMinusCircle} onClick={() => console.log('clicked')}/>
               </div>
               <div className="action">
                 <input
                   name="strength"
                   placeholder="Add more"
                   onChange={e => console.log(e)}
                   onKeyDown={e => console.log(e)}
                 />
                 <FontAwesomeIcon icon={faPlusCircle} onClick={() => console.log('this')}/>
               </div>
             </div> */}
              </Col>
            </FormGroup>
          </div>
          <FormGroup className="d-flex justify-content-between">
            <Button type={'back'} onClick={() => handleSubmit}>
              BACK
            </Button>
            <div>
              <Button type={'back'} className="mx-3" onClick={() => handleSubmit}>
                Save
              </Button>
              <Button type={'back'} onClick={() => console.log('add another')}>
                Save and add another
              </Button>
            </div>
            <Button type={'next'} onClick={() => console.log('add another')}>
              NEXT
            </Button>
          </FormGroup>
        </AvForm>
      </Container>
    </React.Fragment>
  );
};

export const AddInnovation = () => {
  return (
    <InnovationContextProvider>
      <AddInnovationPage />
    </InnovationContextProvider>
  );
};
