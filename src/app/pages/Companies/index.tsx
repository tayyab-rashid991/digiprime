/** @jsxImportSource @emotion/react */
import history from 'app/history';
import { Button } from 'app/components/Form/Button';
import { Header } from 'app/components/Header';
import { CompaniesContext, CompaniesProvider } from 'app/context/CompaniesContext';
import * as React from 'react';
import { Container, FormGroup } from 'reactstrap';
import './index.scss';

const CompaniesPage = () => {
  const { companies } = React.useContext<any>(CompaniesContext);
  return (
    <React.Fragment>
      <Header />
      <Container className="companies">
        <div className="title text-center">
          <h2>Provide your industrial situation to set specific opportunities.</h2>
        </div>
        <div className="roles-table my-4">
          <CompaniesTable data={companies} />
        </div>
        <FormGroup row className="justify-content-between">
          <Button type={'back'} onClick={() => history.push('focus-sectors')}>
            BACK
          </Button>
          {companies.length > 0 && (
            <div>
              <Button className="mx-3" onClick={() => history.push('add-company')}>
                Add companies one by one
              </Button>
              {/* <Button onClick={() => history.push('import-regional-companies')}>Add companies from an excel file</Button> */}
            </div>
          )}
          <div>
            <a href="#" className="skip-btn">
              SKIP
            </a>
            <Button type={'next'} onClick={() => history.push('add-project')}>
              NEXT
            </Button>
          </div>
        </FormGroup>
      </Container>
    </React.Fragment>
  );
};

export const CompaniesTable = ({ data }) => {
  return (
    <table className="table">
      <thead>
        <tr>
          <th>Company name</th>
          <th>NACE Code</th>
          <th>Source</th>
          <th>Position in the circular value-chain </th>
          <th>Application Domain </th>
          <th>Technological capabilities</th>
          <th css={{ background: '#7ECD76' }}>Input Materials/Components</th>
          <th css={{ background: '#7ECD76' }}>Output Materials/Components</th>
          <th css={{ background: '#7ECD76' }}>Not used Materials/Components</th>
        </tr>
      </thead>
      <tr>
        <td colSpan={6}></td>
        <td colSpan={1}>
          <div className="d-flex pt-3 pb-3 px-2 justify-content-between font-weight-bold">
            <span>Type</span>
            <span>Quantity (t/year)</span>
          </div>
        </td>
        <td>
          <div className="d-flex pt-3 pb-3 px-2 justify-content-between font-weight-bold">
            <span>Type</span>
            <span>Quantity (t/year)</span>
          </div>
        </td>
        <td>
          <div className="d-flex pt-3 pb-3 px-2 justify-content-between font-weight-bold">
            <span>Type</span>
            <span>Quantity (t/year)</span>
          </div>
        </td>
      </tr>
      <tbody>
        {data.length > 0 ? (
          data.map((company) => {
            return (
              <tr>
                <td>{company.nameCompany}</td>
                <td>{company?.naceCode?.naceCodeCode}</td>
                <td>{company.companySource}</td>
                <td>
                  {company.positionValueChain1
                    ? `${company.positionValueChain1.positionValueChainDescr}, `
                    : ''}
                  {company.positionValueChain2
                    ? `${company.positionValueChain2.positionValueChainDescr}, `
                    : ''}
                  {company.positionValueChain3
                    ? `${company.positionValueChain3.positionValueChainDescr}`
                    : ''}
                </td>
                <td>
                  {company.applicationDomain1 ? `${company.applicationDomain1.descr}, ` : ''}
                  {company.applicationDomain2 ? `${company.applicationDomain2.descr}, ` : ''}
                  {company.applicationDomain3 ? `${company.applicationDomain3.descr}, ` : ''}
                </td>
                <td>{company.technologicalCap}</td>
                <td>
                  <div className="d-flex pt-3 px-2 justify-content-between">
                    <span>{company.outputDescr}</span>
                    <span>{company.outputQty}</span>
                  </div>
                </td>
                <td>
                  <div className="d-flex pt-3 px-2 justify-content-between">
                    <span>{company.outputDescr}</span>
                    <span>{company.outputQty}</span>
                  </div>
                </td>
                <td>
                  <div className="d-flex pt-3 px-2 justify-content-between">
                    <span>{company.notUsedDescr}</span>
                    <span>{company.notUsedQty}</span>
                  </div>
                </td>
              </tr>
            );
          })
        ) : (
          <tr>
            <td colSpan={9} align={'center'}>
              <div className="action-btn">
                <Button onClick={() => history.push('add-company')}>
                  Add companies <br /> one by one
                </Button>
                {/* <Button onClick={() => history.push('import-regional-companies')}>Add companies <br/> from an excel file</Button> */}
              </div>
            </td>
          </tr>
        )}
      </tbody>
    </table>
  );
};

export const Companies = () => {
  return (
    <CompaniesProvider>
      <CompaniesPage />
    </CompaniesProvider>
  );
};
