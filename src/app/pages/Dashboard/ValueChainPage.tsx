import React, { useEffect, useState } from 'react';
import ErrorBoundary from 'app/utils/ErrorBoundries';
import axios from 'axios';
import DashboardIndustry from 'app/components/DashboardIndustry';
import { API_BASE_URL } from 'app/config';

interface NaceRecord {
  gathering: number;
  primaryMaterial: number;
  production: number;
  packing: number;
  useService: number;
  collection: number;
  disposal: number;
  maintenance: number;
  repair: number;
  reuse: number;
  remanufacturing: number;
  recyclingLoopOpen: number;
  recyclingLoopClose: number;
  biochemicalRecovery: number;
}
interface Data {
  title: string;
  api: string;
  obj: string;
  data: NaceRecord;
  companyCount: number;
}

const NACE_DATA_DEFAULT: NaceRecord = {
  // Headers
  // Main graph
  gathering: 0,
  primaryMaterial: 0,
  production: 0,
  packing: 0,
  useService: 0,
  collection: 0,
  disposal: 0,
  maintenance: 0,
  repair: 0,
  reuse: 0,
  remanufacturing: 0,
  recyclingLoopOpen: 0,
  recyclingLoopClose: 0,
  biochemicalRecovery: 0,
};

const InitNaceData: Data[] = [
  {
    title: 'Argi-food value chain',
    api: `${API_BASE_URL}/naceAgrifoods?size=10000`,
    obj: 'naceAgrifoods',
    data: { ...NACE_DATA_DEFAULT },
    companyCount: 0,
  },
  {
    title: 'Textile value chain',
    api: `${API_BASE_URL}/naceTextiles?size=10000`,
    obj: 'naceTextiles',
    data: { ...NACE_DATA_DEFAULT },
    companyCount: 0,
  },
  {
    title: 'C15 - Manufacture of leather and related products value chain',
    api: `${API_BASE_URL}/naceC15s?size=10000`,
    obj: 'naceC15s',
    data: { ...NACE_DATA_DEFAULT },
    companyCount: 0,
  },
  {
    title:
      'C16 - Manufacture of wood and of products of wood and cork, except furniture; manufacture of articles of straw and plaiting materials value chain',
    api: `${API_BASE_URL}/naceC16s?size=10000`,
    obj: 'naceC16s',
    data: { ...NACE_DATA_DEFAULT },
    companyCount: 0,
  },
  {
    title: 'C17 - Manufacture of paper and paper products - value chain',
    api: `${API_BASE_URL}/naceC17s?size=10000`,
    obj: 'naceC17s',
    data: { ...NACE_DATA_DEFAULT },
    companyCount: 0,
  },
  {
    title: 'C18 - Printing and reproduction of recorded media- value chain',
    api: `${API_BASE_URL}/naceC18s?size=10000`,
    obj: 'naceC18s',
    data: { ...NACE_DATA_DEFAULT },
    companyCount: 0,
  },
  {
    title: 'C19 - Manufacture of coke and refined petroleum products- value chain',
    api: `${API_BASE_URL}/naceC19s?size=10000`,
    obj: 'naceC19s',
    data: { ...NACE_DATA_DEFAULT },
    companyCount: 0,
  },
  {
    title: 'C20 - Manufacture of chemicals and chemical products- value chain',
    api: `${API_BASE_URL}/naceC20s?size=10000`,
    obj: 'naceC20s',
    data: { ...NACE_DATA_DEFAULT },
    companyCount: 0,
  },
  {
    title:
      'C21 - Manufacture of basic pharmaceutical products and pharmaceutical preparations - value chain',
    api: `${API_BASE_URL}/naceC21s?size=10000`,
    obj: 'naceC21s',
    data: { ...NACE_DATA_DEFAULT },
    companyCount: 0,
  },
  {
    title: 'C22 - Manufacture of rubber and plastic product - value chain',
    api: `${API_BASE_URL}/naceC22s?size=10000`,
    obj: 'naceC22s',
    data: { ...NACE_DATA_DEFAULT },
    companyCount: 0,
  },
  {
    title: 'C23 - Manufacture of other non-metallic mineral products - value chain',
    api: `${API_BASE_URL}/naceC23s?size=10000`,
    obj: 'naceC23s',
    data: { ...NACE_DATA_DEFAULT },
    companyCount: 0,
  },
  {
    title: 'C24 - Manufacture of basic metals - value chain',
    api: `${API_BASE_URL}/naceC24s?size=10000`,
    obj: 'naceC24s',
    data: { ...NACE_DATA_DEFAULT },
    companyCount: 0,
  },
  {
    title:
      'C25 - Manufacture of fabricated metal products, except machinery and equipment - value chain',
    api: `${API_BASE_URL}/naceC25s?size=10000`,
    obj: 'naceC25s',
    data: { ...NACE_DATA_DEFAULT },
    companyCount: 0,
  },
  {
    title: 'C26 - Manufacture of computer, electronic and optical product - value chain',
    api: `${API_BASE_URL}/naceC26s?size=10000`,
    obj: 'naceC26s',
    data: { ...NACE_DATA_DEFAULT },
    companyCount: 0,
  },
  {
    title: 'C27 - Manufacture of electrical equipment - value chain',
    api: `${API_BASE_URL}/naceC27s?size=10000`,
    obj: 'naceC27s',
    data: { ...NACE_DATA_DEFAULT },
    companyCount: 0,
  },
  {
    title: 'C28 - Manufacture of machinery and equipment n.e.c- value chain',
    api: `${API_BASE_URL}/naceC28s?size=10000`,
    obj: 'naceC28s',
    data: { ...NACE_DATA_DEFAULT },
    companyCount: 0,
  },
  {
    title: 'C29 - Manufacture of motor vehicles, trailers and semi-trailers- value chain',
    api: `${API_BASE_URL}/naceC29s?size=10000`,
    obj: 'naceC29s',
    data: { ...NACE_DATA_DEFAULT },
    companyCount: 0,
  },
  {
    title: 'C30 - Manufacture of other transport equipment- value chain',
    api: `${API_BASE_URL}/naceC30s?size=10000`,
    obj: 'naceC30s',
    data: { ...NACE_DATA_DEFAULT },
    companyCount: 0,
  },
  {
    title: 'C31 - Manufacture of furniture - value chain',
    api: `${API_BASE_URL}/naceC31s?size=10000`,
    obj: 'naceC31s',
    data: { ...NACE_DATA_DEFAULT },
    companyCount: 0,
  },
  {
    title: 'C32 - Other manufacturing - value chain',
    api: `${API_BASE_URL}/naceC32s?size=10000`,
    obj: 'naceC32s',
    data: { ...NACE_DATA_DEFAULT },
    companyCount: 0,
  },
  {
    title: 'D35 - Electricity, gas, steam and air conditioning supply - value chain',
    api: `${API_BASE_URL}/naceD35s?size=10000`,
    obj: 'naceD35s',
    data: { ...NACE_DATA_DEFAULT },
    companyCount: 0,
  },
];

export const Dashboard = () => {
  const [naceList, setNaceList] = useState<Data[]>(InitNaceData);

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fetchData = async () => {
    const newNaceList = [...naceList];

    await Promise.all(
      naceList.map(async (nace, i) => {
        try {
          const response = await axios(nace.api);
          const naceRecords = (response.data._embedded as any)[nace.obj];

          const data = await aggregateRecord(naceRecords);

          newNaceList[i] = {
            ...naceList[i],
            data,
            companyCount: naceRecords.length,
          };
        } catch (error) {
          console.log(error);
        }
      }),
    );

    console.log('done');
    setNaceList(newNaceList);
  };

  const aggregateRecord = (naceRecords): any => {
    return new Promise((resolve) => {
      let countData = { ...NACE_DATA_DEFAULT };

      naceRecords.forEach((value) => {
        Object.keys(value).forEach((key: string) => {
          if (value[key]) {
            countData = {
              ...countData,
              [key]: countData[key] + 1,
            };
          }
        });
      });
      resolve(countData);
    });
  };

  return (
    <ErrorBoundary>
      {naceList.length > 0 &&
        naceList.map((naceData: Data, index: number) => {
          return (
            <div key={index}>
              <DashboardIndustry
                // Header
                index={index + 1}
                title={naceData.title}
                companyCount={naceData.companyCount}
                // Body
                gathering={naceData.data.gathering}
                primaryMaterial={naceData.data.primaryMaterial}
                production={naceData.data.production}
                packing={naceData.data.packing}
                useService={naceData.data.useService}
                collection={naceData.data.collection}
                disposal={naceData.data.disposal}
                maintenance={naceData.data.maintenance}
                repair={naceData.data.repair}
                reuse={naceData.data.reuse}
                remanufacturing={naceData.data.remanufacturing}
                recyclingLoopOpen={naceData.data.recyclingLoopOpen}
                recyclingLoopClose={naceData.data.recyclingLoopClose}
                biochemicalRecovery={naceData.data.biochemicalRecovery}
              />
            </div>
          );
        })}
    </ErrorBoundary>
  );
};
