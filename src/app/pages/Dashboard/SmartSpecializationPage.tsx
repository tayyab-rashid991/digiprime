/** @jsxImportSource @emotion/react */
import * as React from 'react';
import { Button } from 'app/components/Form/Button';
import { SmartSpecializationTable } from '../S3Details';
import {
  SmartSpecializationContext,
  SmartSpecializationProvider,
} from 'app/context/SmartSpecializationContext';
import { css } from '@emotion/react';

const SmartSpecialization = () => {
  const { smartSpecializationData } = React.useContext<any>(SmartSpecializationContext);
  return (
    <React.Fragment>
      <div
        css={css`
          padding: 30px;
          table th {
            font-size: 14px;
          }
        `}
      >
        <SmartSpecializationTable data={smartSpecializationData} />
        <div
          css={css`
            display: flex;
            align-items: center;
            width: 100%;
            justify-content: center;
          `}
        >
          <Button
            type="button"
            onClick={() => (window.location.href = '/add-smart-specialization')}
          >
            ADD S3
          </Button>
        </div>
      </div>
    </React.Fragment>
  );
};

export const SmartSpecializationPage = () => {
  return (
    <SmartSpecializationProvider>
      <SmartSpecialization />
    </SmartSpecializationProvider>
  );
};
