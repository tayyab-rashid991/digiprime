/** @jsxImportSource @emotion/react */
import * as React from 'react';
import { CompaniesTable } from '../Companies';
import { Button } from 'app/components/Form/Button';
import { CompaniesProvider, CompaniesContext } from 'app/context/CompaniesContext';
import { css } from '@emotion/react';
import history from 'app/utils/history';

const Companies = () => {
  const { companies } = React.useContext<any>(CompaniesContext);
  return (
    <div
      className="companies"
      css={css`
        padding: 15px;
        width: 100%;
        overflow: hidden;
      `}
    >
      <CompaniesTable data={companies} />
      <div className="action-btn">
        <Button onClick={() => history.push('add-company')}>Invite Your Companies</Button>
        <Button onClick={() => history.push('import-regional-companies')}>Add a company</Button>
      </div>
    </div>
  );
};

export const CompaniesPage = () => {
  return (
    <CompaniesProvider>
      <Companies />
    </CompaniesProvider>
  );
};
