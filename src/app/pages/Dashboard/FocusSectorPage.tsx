/** @jsxImportSource @emotion/react */
import * as React from 'react';
import { FocusTable } from '../Step2-1';
import { Button } from 'app/components/Form/Button';
import {
  AddFocusSectorContextProvider,
  AddFocusSectorContext,
} from 'app/context/AddFocusSectorContext';
import { css } from '@emotion/react';

const FocusSector = () => {
  const { sectors } = React.useContext<any>(AddFocusSectorContext);
  return (
    <React.Fragment>
      <div
        css={css`
          padding: 30px;
          table th {
            font-size: 14px;
          }
        `}
      >
        <FocusTable data={sectors} />
      </div>
      <div
        css={css`
          display: flex;
          align-items: center;
          width: 100%;
          justify-content: center;
        `}
      >
        <Button type="button" onClick={() => (window.location.href = '/add-focus-sector')}>
          ADD A SECTOR
        </Button>
      </div>
    </React.Fragment>
  );
};

export const FocusSectorPage = () => {
  return (
    <AddFocusSectorContextProvider>
      <FocusSector />
    </AddFocusSectorContextProvider>
  );
};
