import * as React from 'react';
import { Switch } from 'react-router-dom';

// Pages
import { LandingPage } from 'app/pages/LandingPage';
import { AddCompanyPage } from 'app/pages/AddCompany';
import { AddRegionalContact } from 'app/pages/AddRegionalContact';
import { AddSmartSpecialization } from 'app/pages/AddSmartSpecialization';
import { Regions } from 'app/pages/Regions';
import { S3Details } from 'app/pages/S3Details';
import { SWOTAnalysis } from 'app/pages/SWOTAnalysis';
import { AddFocusSector } from 'app/pages/AddFocusSector';
import { FocusSectors } from 'app/pages/Step2-1';
import { Companies } from 'app/pages/Companies';
import { ImportRegionalCompanies } from 'app/pages/ImportRegionalCompanies';
import { CapabilitiesList } from 'app/pages/CapabilitiesList';
import { AddProject } from 'app/pages/AddProject';
import { AddInnovation } from 'app/pages/AddInnovation';
import { InnovationOverview } from 'app/pages/InnovationOverview';
import { FocusSector } from 'app/pages/FocusSector';
import { FinalPage } from 'app/pages/FinalPage';
import { RNDCapabilities } from 'app/pages/R&DCapabilities';
import { RnDList } from './RndList';
import { EducationalCapabilities } from 'app/pages/EducationalCapabilities';
import { EducationalCapabilitiesOverview } from 'app/pages/EducationalCapabilitiesOverview';
import { EmergingIdeas } from 'app/pages/EmergingIdeas';
import { EmergingIdeasOverview } from 'app/pages/EmergingIdeasOverview';
import { AddExistingCirEcoLeg } from 'app/pages/ExistingCircEcoLegislation';
import { CircEcoLegOverviewPage } from 'app/pages/ExistingCircEcoLegislation/CircEcoLegistlationOverview';
import { ExistingFunding } from 'app/pages/ExistingFunding';
import { FundingOverview } from 'app/pages/ExistingFunding/fundingOverview';
import { Hotspot } from 'app/pages/Hotspots';
import { HotspotOverview } from './Hotspots/overview';
import { Login } from 'app/pages/Login';

// Dashboard Pages
// import { Dashboard } from './Dashboard';
// import { SmartSpecializationPage } from './Dashboard/SmartSpecializationPage';
// import { FocusSectorPage } from './Dashboard/FocusSectorPage';
// import { CompaniesPage } from './Dashboard/CompaniesPage';
// Layouts
import UnauthenticatedLayout from '../Layouts/UnAuthenticatedLayout';
import { AuthenticatedLayout } from '../Layouts/AuthenticatedLayout';
import DashboardLayout from 'app/Layouts/DashboardLayout';
import { Dashboard } from './Dashboard/ValueChainPage';
// import DashboardLayout from '../Layouts/DashboardLayout';

export const Pages = () => {
  return (
    <Switch>
      <AuthenticatedLayout path="/" exact={true} component={LandingPage} />
      <AuthenticatedLayout path="/add-company" exact={true} component={AddCompanyPage} />
      <AuthenticatedLayout
        path="/add-regional-contact"
        exact={true}
        component={AddRegionalContact}
      />
      <AuthenticatedLayout path="/regions" exact={true} component={Regions} />
      <AuthenticatedLayout
        path="/add-smart-specialization"
        exact={true}
        component={AddSmartSpecialization}
      />
      <AuthenticatedLayout path="/s3-details" exact={true} component={S3Details} />
      <AuthenticatedLayout path="/swot-analysis" exact={true} component={SWOTAnalysis} />
      <AuthenticatedLayout path="/add-focus-sector" exact={true} component={AddFocusSector} />
      <AuthenticatedLayout path="/focus-sectors" exact={true} component={FocusSectors} />
      <AuthenticatedLayout path="/companies" exact={true} component={Companies} />
      <AuthenticatedLayout
        path="/import-regional-companies"
        exact={true}
        component={ImportRegionalCompanies}
      />
      <AuthenticatedLayout path="/capabilities" exact={true} component={CapabilitiesList} />
      <AuthenticatedLayout path="/add-project" exact={true} component={AddProject} />
      <AuthenticatedLayout path="/add-innovation" exact={true} component={AddInnovation} />
      <AuthenticatedLayout
        path="/innovation-overview"
        exact={true}
        component={InnovationOverview}
      />
      <AuthenticatedLayout path="/focus-sector-2-1" exact={true} component={FocusSector} />
      <AuthenticatedLayout path="/complete-mapping" exact={true} component={FinalPage} />
      <AuthenticatedLayout path="/rnd-capabilities" exact={true} component={RNDCapabilities} />
      <AuthenticatedLayout path="/rnd-list" exact={true} component={RnDList} />
      <AuthenticatedLayout
        path="/educational-capabilities"
        exact={true}
        component={EducationalCapabilities}
      />
      <AuthenticatedLayout
        path="/educational-capabilities-overview"
        exact={true}
        component={EducationalCapabilitiesOverview}
      />
      <AuthenticatedLayout path="/emerging-ideas" exact={true} component={EmergingIdeas} />
      <AuthenticatedLayout
        path="/emerging-ideas-overview"
        exact={true}
        component={EmergingIdeasOverview}
      />
      <AuthenticatedLayout
        path="/add-exist-cir-eco-leg"
        exact={true}
        component={AddExistingCirEcoLeg}
      />
      <AuthenticatedLayout
        path="/cir-eco-leg-overview"
        exact={true}
        component={CircEcoLegOverviewPage}
      />
      <AuthenticatedLayout path="/existing-funding" exact={true} component={ExistingFunding} />
      <AuthenticatedLayout path="/funding-overview" exact={true} component={FundingOverview} />
      <AuthenticatedLayout path="/hotspot" exact={true} component={Hotspot} />
      <AuthenticatedLayout path="/hotspot-overview" exact={true} component={HotspotOverview} />

      <DashboardLayout path="/dashboard/value-chains" exact={true} component={Dashboard} />
      {/* <DashboardLayout path="/dashboard/smart-specialization" exact={true} component={SmartSpecializationPage}/>
      <DashboardLayout path="/dashboard/focus-sector" exact={true} component={FocusSectorPage}/>
      <DashboardLayout path="/dashboard/companies" exact={true} component={CompaniesPage}/> */}
      <UnauthenticatedLayout path="/login" exact={true} component={Login} />
      {/* Dashboard Pages */}

      {/* <Redirect to="/" /> */}
    </Switch>
  );
};

export default Pages;
