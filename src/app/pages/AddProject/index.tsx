import './index.scss';

import * as React from 'react';
import { Header } from 'app/components/Header';
import { Col, Container, FormGroup, Label } from 'reactstrap';
import { Multiselect } from 'multiselect-react-dropdown';
import { Button } from 'app/components/Form/Button';
import { NaceCodeSelect } from 'app/components/Form/NaceCodeDropdown';
import { ProjectContextProvider, ProjectContext } from 'app/context/ProjectContext';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import history from 'app/history';

const AddProjectPage = () => {
  const region = JSON.parse(localStorage.getItem('region') || JSON.stringify({}) || JSON.stringify({}));
  const initialState = {
    program: 0,
    projectName: '',
    totalBudget: '',
    consortium: '',
    description: '',
    source: {},
    naceCode: {},
    ...region,
  };
  const { sources, addProject } = React.useContext<any>(ProjectContext);
  const [inputs, setInputs] = React.useState(initialState);
  const onSelect = (selectedList, selectedItem, name) => {
    delete selectedItem._links;
    setInputs((inputs) => ({ ...inputs, [name]: selectedItem }));
  };
  const handleInputChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
    const el = e.target as HTMLInputElement;
    setInputs((inputs) => ({ ...inputs, [el.name]: el.value }));
  };
  const handleSubmit = async () => {
    const success = await addProject(inputs);
    if (success) {
      history.push('/capabilities');
    }
  };
  return (
    <React.Fragment>
      <Header />
      <Container className="add-project">
        <div className="title text-center">
          <h2>Add a Project</h2>
        </div>
        <div className="add-project-form">
          <AvForm>
            <FormGroup row>
              <Label sm={4}>Project Name</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="projectName"
                  id="projectName"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={inputs.projectName}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter project name',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Funding Source</Label>
              <Col sm={8}>
                <Multiselect
                  options={sources}
                  displayValue="sourceDescr"
                  showCheckbox={true}
                  singleSelect
                  onSelect={(selectedList, selectedItem) =>
                    onSelect(selectedList, selectedItem, 'source')
                  }
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Project Abstract</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="description"
                  id="description"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={inputs.description}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter project abstract',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Total Budget</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="totalBudget"
                  id="totalBudget"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={inputs.totalBudget}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter total budget',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Consortium</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="consortium"
                  id="consortium"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={inputs.consortium}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter consortium',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Target Sector</Label>
              <Col sm={8}>
                <NaceCodeSelect
                  onSelect={(selectedList, selectedItem) =>
                    onSelect(selectedList, selectedItem, 'naceCode')
                  }
                />
              </Col>
            </FormGroup>
          </AvForm>
        </div>
        <FormGroup row className="justify-content-between">
          <Button type={'back'} onClick={() => history.push('capabilities')}>
            BACK
          </Button>
          <div>
            <Button type={'submit'} className="mr-3" onClick={handleSubmit}>
              SAVE
            </Button>
            <Button type={'button'} onClick={() => console.log('clicked')}>
              Save and add another
            </Button>
          </div>
        </FormGroup>
      </Container>
    </React.Fragment>
  );
};

export const AddProject = () => {
  return (
    <ProjectContextProvider>
      <AddProjectPage />
    </ProjectContextProvider>
  );
};
