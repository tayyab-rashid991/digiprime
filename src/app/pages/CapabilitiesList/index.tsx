/** @jsxImportSource @emotion/react */
import history from 'app/history';
import { Button } from 'app/components/Form/Button';
import { Header } from 'app/components/Header';
import { ProjectContext, ProjectContextProvider } from 'app/context/ProjectContext';
import * as React from 'react';
import { Container, FormGroup, Modal, ModalBody, ModalHeader } from 'reactstrap';
import './index.scss';

const Capabilities = () => {
  const [modal, setModal] = React.useState(false);
  const { capabilities } = React.useContext<{ capabilities: any }>(ProjectContext as any);
  const toggle = () => setModal(!modal);
  return (
    <div>
      <Header />
      <Container className="capabilities">
        <div className="title text-center">
          <h2>
            Create a circular economy related overview of R&D, innovation and education capabilities
          </h2>
        </div>
        <div className="capabilities-table my-4">
          <table className="table">
            <tr>
              <td colSpan={7}>R&D, Innovation and Education capabilities: general overview</td>
            </tr>
            <tr>
              <td colSpan={4}>Objectives</td>
              <td colSpan={3}>Outputs</td>
            </tr>
            <tr>
              <td css={{ width: '18%' }}>
                <label>Project Name</label>
                <p>(past and ongoing projects)</p>
              </td>
              <td css={{ width: '8%' }}>
                <label>
                  Funding <br /> Source
                </label>
              </td>
              <td css={{ width: '10%' }}>
                <label>
                  Funding <br /> Program
                </label>
              </td>
              <td css={{ width: '24%' }}>
                <label>
                  Project <br /> Abstract
                </label>
              </td>
              <td css={{ width: '10%' }}>
                <label>
                  Total <br /> Budget (M€)
                </label>
              </td>
              <td css={{ width: '20%' }}>
                <div className="justify-content-between d-flex align-items-center">
                  <label>Consortium</label>
                  <label>Regional</label>
                </div>
              </td>
              <td css={{ width: '10%' }}>
                <label>
                  Target <br /> Sectors(s)
                </label>
              </td>
            </tr>
            <tbody>
              {capabilities.map((capability) => {
                return (
                  <tr>
                    <td>
                      <p>{capability.projectName}</p>
                    </td>
                    <td>
                      <p>EU National Regional Others</p>
                    </td>
                    <td>
                      <p>{capability.program}</p>
                    </td>
                    <td>
                      <div className="content">
                        <p>{capability.description}</p>
                        <a onClick={() => toggle()}>Read More</a>
                      </div>
                    </td>
                    <td>
                      <p>{capability.totalBudget}</p>
                    </td>
                    <td>
                      <div className="consortium">
                        <ul>
                          <li>
                            <span>{capability.consortium}</span>
                            {/*<FormGroup check>*/}
                            {/*<Label check for="Instituto">*/}
                            {/*Instituto Politecnico de Coimbra (Portugal)*/}
                            {/*</Label>*/}
                            {/*<Input type="checkbox" id="Instituto"/>*/}
                            {/*</FormGroup>*/}
                          </li>
                        </ul>
                        <div className="action">
                          <input
                            name="strength"
                            placeholder="Add more"
                            value={''}
                            onChange={(e) => console.log(e)}
                            onKeyDown={(e) => console.log(e)}
                          />
                        </div>
                      </div>
                    </td>
                    <td>
                      <p>A1 - Crop and animal production, hunting and related service activities</p>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
        <FormGroup row className="justify-content-between">
          <Button type={'back'} onClick={() => history.push('companies')}>
            BACK
          </Button>
          <Button type={'button'} onClick={() => history.push('add-project')}>
            Add a Project
          </Button>
          <div>
            <a className="skip-btn" onClick={() => history.push('add-innovation')}>
              SKIP
            </a>
            <Button type={'next'} onClick={() => history.push('add-innovation')}>
              NEXT
            </Button>
          </div>
        </FormGroup>
      </Container>
      <Modal isOpen={modal} toggle={toggle} className="modal-lg">
        <ModalHeader toggle={toggle}>Full Description</ModalHeader>
        <ModalBody>
          <p>
            Municipal waste management practices are failing to achieve high recycling rates in
            several southern European countries, making it difficult to achieve EU targets. The
            Waste Framework Directive requires Member States to increase the re-use and recycling of
            waste materials, such as paper, metal, plastic and glass from households, to a minimum
            of 50% overall by weight by 2020. Recycling rates of these waste streams in Greece,
            Cyprus and Portugal are currently around 20%, well below the target and the current
            European average (35%). One cause of this is inefficiency in the selective collection of
            materials: less than 15% of all lightweight packaging is presently recovered
          </p>
        </ModalBody>
      </Modal>
    </div>
  );
};

export const CapabilitiesList = () => {
  return (
    <ProjectContextProvider>
      <Capabilities />
    </ProjectContextProvider>
  );
};
