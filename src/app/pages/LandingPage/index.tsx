import './index.scss';
import * as React from 'react';
import { Container, Col } from 'reactstrap';
import history from '../../history';

export const LandingPage = () => {
  const businesses = [
    { img: 'business-modal', url: 'regions', label: 'Regional Officer' },
    { img: 'business-modal2', url: 'add-company', label: 'Company User' },
  ];
  return (
    <div className="landing-page">
      <Container>
        <div className="logo text-center">
          <a href="#">
            <img src={`${process.env.PUBLIC_URL}/assets/img/logo.png`} />
          </a>
          <p>
            T3.6 Identification of cross-regional value-chains <br /> through open innovation
          </p>
        </div>
        <div className="title">
          <h2>
            Welcome in the Service 3.6 Mapping tool <br />
            Please select who you are
          </h2>
        </div>
        <div className="row business-modal">
          {businesses.map((business, i) => {
            return (
              <Col key={i}>
                <div className="business d-flex justify-content-center">
                  <a onClick={() => history.push(business.url)}>
                    <img src={`${process.env.PUBLIC_URL}/assets/img/${business.img}.png`} />
                  </a>
                </div>
                <label>{business.label}</label>
              </Col>
            );
          })}
        </div>
      </Container>
    </div>
  );
};
