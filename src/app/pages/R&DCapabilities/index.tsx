import history from 'app/history';
import { AvField, AvForm } from 'availity-reactstrap-validation';
import { ApplicationDomainSelect } from 'app/components/Form/ApplicationDomain';
import { Button } from 'app/components/Form/Button';
import { Header } from 'app/components/Header';
import { RndContext, RndContextProvider } from 'app/context/RnDContext';
import * as React from 'react';
import { Col, Container, FormGroup, Label } from 'reactstrap';

const RNDCapabilitiesPage = () => {
  const { postResearchCapability } = React.useContext<any>(RndContext);
  const initialState = {
    ipcCode: '',
    department: '',
    name: '',
    nresearchers: 0,
    nstartUp: 0,
    researchDevCapabilityId: 0,
  };
  const [inputs, setInputs] = React.useState(initialState);
  const handleInputChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
    const el = e.target as HTMLInputElement;
    setInputs((inputs) => ({ ...inputs, [el.name]: el.value }));
  };
  const handleSubmit = async () => {
    const selectedRegion = JSON.parse(localStorage.getItem('region') || JSON.stringify({}));
    const obj = { ...inputs, region: selectedRegion['region'] };
    const success = await postResearchCapability(obj);
    if (success) {
      console.log('successfully added ');
      history.push('rnd-list');
    } else {
      console.log('error ');
    }
  };
  const onSelect = (selectedList, selectedItem, name) => {
    delete selectedItem._links;
    setInputs((inputs) => ({ ...inputs, [name]: selectedItem }));
  };
  return (
    <React.Fragment>
      <Header />
      <Container className="add-project">
        <AvForm onSubmit={handleSubmit}>
          <div className="title text-center">
            <h2>R&D Capabilities</h2>
          </div>
          <div className="add-project-form">
            <FormGroup row>
              <Label sm={4}>Name of Research Centre/University</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="name"
                  value={inputs.name}
                  id="name"
                  onChange={handleInputChange}
                  required
                  errorMessage=""
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter research centre/university',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Departments</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="department"
                  onChange={handleInputChange}
                  id="department"
                  required
                  value={inputs.department}
                  errorMessage=""
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter department',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Number of researchers</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="nresearchers"
                  id="nresearchers"
                  required
                  value={inputs.nresearchers}
                  errorMessage=""
                  onChange={handleInputChange}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter researcher',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Application Domain</Label>
              <Col sm={8}>
                <ApplicationDomainSelect
                  onSelect={(selectedList, selectedItem) =>
                    onSelect(selectedList, selectedItem, 'applicationDomain')
                  }
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>IPC Codes</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="ipcCode"
                  id="ipcCode"
                  required
                  onChange={handleInputChange}
                  value={inputs.ipcCode}
                  errorMessage=""
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter IPC codes',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Number of Spin-off or start-up</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="nstartUp"
                  id="nstartUp"
                  required
                  onChange={handleInputChange}
                  value={inputs.nstartUp}
                  errorMessage=""
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter number of startup',
                    },
                  }}
                />
              </Col>
            </FormGroup>
          </div>
          <FormGroup className="d-flex justify-content-between">
            <Button type={'back'} onClick={() => handleSubmit}>
              BACK
            </Button>
            <div>
              <Button type={'back'} className="mx-3" onClick={() => handleSubmit}>
                Save
              </Button>
              <Button type={'back'} onClick={() => console.log('add another')}>
                Save and add another
              </Button>
            </div>
            <Button type={'next'} onClick={() => history.push('rnd-list')}>
              NEXT
            </Button>
          </FormGroup>
        </AvForm>
      </Container>
    </React.Fragment>
  );
};

export const RNDCapabilities = () => {
  return (
    <RndContextProvider>
      <RNDCapabilitiesPage />
    </RndContextProvider>
  );
};
