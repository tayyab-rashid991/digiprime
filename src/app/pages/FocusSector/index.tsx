import history from 'app/history';
import { AvField, AvForm } from 'availity-reactstrap-validation';
import { Button } from 'app/components/Form/Button';
import { Header } from 'app/components/Header';
import * as React from 'react';
import { Col, Container, FormGroup, Row } from 'reactstrap';
import './index.scss';

const FocusSectorPage = () => {
  const [, setInputs] = React.useState();
  const handleSubmit = () => {
    history.push('add-project');
  };
  const handleInputChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
    const el = e.target as HTMLInputElement;
    setInputs((inputs: any) => ({ ...inputs, [el.name]: el.value }));
  };
  return (
    <React.Fragment>
      <Header />
      <div className="title text-center">
        <h2>
          Create a circular economy related overview of R&D, innovation and education capabilities
        </h2>
      </div>
      <AvForm onSubmit={handleSubmit}>
        <Container className="focus-sector">
          <div className="form">
            <Row className="justify-content-center">
              <div className="title text-center">
                <h2>Expenditure of R&D</h2>
              </div>
            </Row>
            <Row>
              <Col>
                <AvField
                  type="text"
                  className="form-control"
                  name="nameCompany"
                  id="nameCompany"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={''}
                  validate={{
                    required: {
                      value: true,
                    },
                  }}
                />
              </Col>
              <Col>
                <AvField
                  type="text"
                  className="form-control"
                  name="nameCompany"
                  id="nameCompany"
                  required
                  errorMessage=""
                  placeholder={'Resource'}
                  onChange={handleInputChange}
                  value={''}
                  validate={{
                    required: {
                      value: true,
                    },
                  }}
                />
              </Col>
            </Row>
            <Row className="justify-content-center">
              <div className="title text-center">
                <h2>Research and Innovation</h2>
              </div>
            </Row>
            <Row>
              <Col>
                <AvField
                  type="text"
                  className="form-control"
                  name="nameCompany"
                  id="nameCompany"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={''}
                  validate={{
                    required: {
                      value: true,
                    },
                  }}
                />
              </Col>
              <Col>
                <AvField
                  type="text"
                  className="form-control"
                  name="nameCompany"
                  id="nameCompany"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={''}
                  validate={{
                    required: {
                      value: true,
                    },
                  }}
                />
              </Col>
            </Row>
            <Row className="justify-content-center">
              <div className="title text-center">
                <h2>
                  Number of total patent applications on the <br />
                  selected enabling technologies
                </h2>
              </div>
            </Row>
            <Row>
              <Col>
                <AvField
                  type="text"
                  className="form-control"
                  name="nameCompany"
                  id="nameCompany"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={''}
                  validate={{
                    required: {
                      value: true,
                    },
                  }}
                />
              </Col>
              <Col>
                <AvField
                  type="text"
                  className="form-control"
                  name="nameCompany"
                  id="nameCompany"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={''}
                  validate={{
                    required: {
                      value: true,
                    },
                  }}
                />
              </Col>
            </Row>
            <Row className="justify-content-center">
              <div className="title text-center">
                <h2>People employed in R&D</h2>
              </div>
            </Row>
            <Row>
              <Col>
                <AvField
                  type="text"
                  className="form-control"
                  name="nameCompany"
                  id="nameCompany"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={''}
                  validate={{
                    required: {
                      value: true,
                    },
                  }}
                />
              </Col>
              <Col>
                <AvField
                  type="text"
                  className="form-control"
                  name="nameCompany"
                  id="nameCompany"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={''}
                  validate={{
                    required: {
                      value: true,
                    },
                  }}
                />
              </Col>
            </Row>
          </div>
          <FormGroup className="d-flex justify-content-between">
            <Button type={'back'} onClick={() => history.push('companies')}>
              BACK
            </Button>
            <Button type={'next'} onClick={() => handleSubmit}>
              SAVE
            </Button>
          </FormGroup>
        </Container>
      </AvForm>
    </React.Fragment>
  );
};

export const FocusSector = () => {
  return <FocusSectorPage />;
};
