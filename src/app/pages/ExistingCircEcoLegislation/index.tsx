import history from 'app/history';
import { AvField, AvForm } from 'availity-reactstrap-validation';
import { Button } from 'app/components/Form/Button';
import { FormWrapper } from 'app/components/FormWrapper';
import { Header } from 'app/components/Header';
import {
  ExisCircEcoLegContext,
  ExisCircEcoLegContextProvider,
} from 'app/context/ExisCircEcoLegContext';
import * as React from 'react';
import { Col, Container, FormGroup, Label } from 'reactstrap';

const AddExistingCirEcoLegPage = () => {
  const { addEcoLegislation } = React.useContext<any>(ExisCircEcoLegContext);
  const initialState = {
    regionalExistingName: '',
    regionalUnderDevelopment: '',
    nationalExistingName: '',
    nationalUnderDevelopment: '',
    educationActions: '',
    collaborationPlatforms: '',
    businessSupportScheme: '',
    incentiveMechanism: '',
  };
  const [inputs, setInputs] = React.useState(initialState);
  const handleSubmit = async () => {
    const regionUrl = JSON.parse(localStorage.getItem('region') || JSON.stringify({}));
    const obj = {
      ...inputs,
      region: `https://digiprime-veltha-services.red.extrasys.it/regions/${regionUrl['region']['regionId']}`,
    };
    const success = await addEcoLegislation(obj);
    if (success) {
      history.push('cir-eco-leg-overview');
    } else {
    }
  };
  const handleInputChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
    const el = e.target as HTMLInputElement;
    setInputs((inputs) => ({ ...inputs, [el.name]: el.value }));
  };
  return (
    <React.Fragment>
      <Header />
      <Container className="add-focus-sector">
        <div className="title text-center">
          <h2>Existing Circular Economy Legislation</h2>
        </div>
        <AvForm onSubmit={handleSubmit}>
          <div className="add-company-form">
            <FormWrapper>
              <FormGroup row>
                <Label sm={4}>Regional Existing Name</Label>
                <Col sm={8}>
                  <AvField
                    type="text"
                    className="form-control"
                    name="regionalExistingName"
                    id="regionalExistingName"
                    required
                    errorMessage=""
                    onChange={handleInputChange}
                    value={inputs.regionalExistingName}
                    validate={{
                      required: {
                        value: true,
                        errorMessage: 'Please enter regional existing name',
                      },
                    }}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label sm={4}>Regional Under Development Name</Label>
                <Col sm={8}>
                  <AvField
                    type="text"
                    className="form-control"
                    name="regionalUnderDevelopment"
                    id="regionalUnderDevelopment"
                    required
                    errorMessage=""
                    onChange={handleInputChange}
                    value={inputs.regionalUnderDevelopment}
                    validate={{
                      required: {
                        value: true,
                        errorMessage: 'Please enter regional under development name',
                      },
                    }}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label sm={4}>National Existing Name</Label>
                <Col sm={8}>
                  <AvField
                    type="text"
                    className="form-control"
                    name="nationalExistingName"
                    id="nationalExistingName"
                    required
                    errorMessage=""
                    onChange={handleInputChange}
                    value={inputs.nationalExistingName}
                    validate={{
                      required: {
                        value: true,
                        errorMessage: 'Please enter national existing name',
                      },
                    }}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label sm={4}>National Under Development Name</Label>
                <Col sm={8}>
                  <AvField
                    type="text"
                    className="form-control"
                    name="nationalUnderDevelopment"
                    id="nationalUnderDevelopment"
                    required
                    errorMessage=""
                    onChange={handleInputChange}
                    value={inputs.nationalUnderDevelopment}
                    validate={{
                      required: {
                        value: true,
                        errorMessage: 'Please enter national under development name',
                      },
                    }}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label sm={4}>Education, Information and Awareness Actions</Label>
                <Col sm={8}>
                  <AvField
                    type="text"
                    className="form-control"
                    name="educationActions"
                    id="educationActions"
                    required
                    errorMessage=""
                    onChange={handleInputChange}
                    value={inputs.educationActions}
                    validate={{
                      required: {
                        value: true,
                        errorMessage: 'Please enter education actions',
                      },
                    }}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label sm={4}>Collaboration Platforms</Label>
                <Col sm={8}>
                  <AvField
                    type="text"
                    className="form-control"
                    name="collaborationPlatforms"
                    id="collaborationPlatforms"
                    required
                    errorMessage=""
                    onChange={handleInputChange}
                    value={inputs.collaborationPlatforms}
                    validate={{
                      required: {
                        value: true,
                        errorMessage: 'Please enter collaboration platforms',
                      },
                    }}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label sm={4}>Business support schemes</Label>
                <Col sm={8}>
                  <AvField
                    type="text"
                    className="form-control"
                    name="businessSupportScheme"
                    id="businessSupportScheme"
                    required
                    errorMessage=""
                    onChange={handleInputChange}
                    value={inputs.businessSupportScheme}
                    validate={{
                      required: {
                        value: true,
                        errorMessage: 'Please enter business support scheme',
                      },
                    }}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label sm={4}>Incentive mechanisms for Circular Economy</Label>
                <Col sm={8}>
                  <AvField
                    type="text"
                    className="form-control"
                    name="incentiveMechanism"
                    id="incentiveMechanism"
                    required
                    errorMessage=""
                    onChange={handleInputChange}
                    value={inputs.incentiveMechanism}
                    validate={{
                      required: {
                        value: true,
                        errorMessage: 'Please enter incentive mechanisms',
                      },
                    }}
                  />
                </Col>
              </FormGroup>
            </FormWrapper>
          </div>
          <FormGroup className="d-flex justify-content-between">
            <Button type={'back'} onClick={() => history.push('')}>
              BACK
            </Button>
            <div>
              <Button type={'back'} className="mx-3" onClick={() => handleSubmit}>
                Save
              </Button>
              <Button type={'back'} onClick={() => console.log('add another')}>
                Save and add another
              </Button>
            </div>
            <Button type={'next'} onClick={() => console.log('add another')}>
              NEXT
            </Button>
          </FormGroup>
        </AvForm>
      </Container>
    </React.Fragment>
  );
};

export const AddExistingCirEcoLeg = () => {
  return (
    <ExisCircEcoLegContextProvider>
      <AddExistingCirEcoLegPage />
    </ExisCircEcoLegContextProvider>
  );
};
