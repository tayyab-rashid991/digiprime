import history from 'app/history';
import { Button } from 'app/components/Form/Button';
import { Header } from 'app/components/Header';
import {
  ExisCircEcoLegContext,
  ExisCircEcoLegContextProvider,
} from 'app/context/ExisCircEcoLegContext';
import * as React from 'react';
import { Container, FormGroup } from 'reactstrap';

const CircEcoLegOverview = () => {
  const { existingCeLegislations } = React.useContext<any>(ExisCircEcoLegContext);
  return (
    <React.Fragment>
      <Header />
      <Container className={'companies'}>
        <div className="title text-center">
          <h2>Existing Circular Economy Legislation</h2>
        </div>
        <div className="roles-table my-4">
          <table className="table">
            <thead>
              <tr>
                <th>Regional Existing Name</th>
                <th>Regional Under Development Name</th>
                <th>National Existing Name</th>
                <th>National Under Development Name </th>
                <th>Education, Information and Awareness Actions</th>
                <th>Collaboration Platforms</th>
                <th>Business support schemes</th>
                <th>Incentive mechanisms for Circular Economy</th>
              </tr>
            </thead>
            <tbody>
              {existingCeLegislations.map((item, i) => {
                return (
                  <tr key={i}>
                    <td>{item.regionalExistingName}</td>
                    <td>{item.regionalUnderDevelopment}</td>
                    <td>{item.nationalExistingName}</td>
                    <td>{item.nationalUnderDevelopment}</td>
                    <td>{item.educationActions}</td>
                    <td>{item.collaborationPlatforms}</td>
                    <td>{item.businessSupportScheme}</td>
                    <td>{item.incentiveMechanism}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
        <FormGroup row className="justify-content-between">
          <Button type={'back'} onClick={() => history.push('add-exist-cir-eco-leg')}>
            BACK
          </Button>
          <Button type={'button'} onClick={() => history.push('add-exist-cir-eco-leg')}>
            Add legislation
          </Button>
          <div>
            <Button type={'next'} onClick={() => history.push('existing-funding')}>
              NEXT
            </Button>
          </div>
        </FormGroup>
      </Container>
    </React.Fragment>
  );
};

export const CircEcoLegOverviewPage = () => {
  return (
    <ExisCircEcoLegContextProvider>
      <CircEcoLegOverview />
    </ExisCircEcoLegContextProvider>
  );
};
