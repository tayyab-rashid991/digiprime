import * as React from 'react';
import { Container, FormGroup } from 'reactstrap';
import {
  EducationalCapabilitiesContext,
  EducationalCapabilitiesContextProvider,
} from 'app/context/EducationalCapabilitiesContext';
import { Header } from 'app/components/Header';
import { Button } from 'app/components/Form/Button';
import history from 'app/history';

const EducationalCapabilitiesOverviewPage = () => {
  const { eduCapabilities } = React.useContext<any>(EducationalCapabilitiesContext);
  return (
    <React.Fragment>
      <Header />
      <Container className="companies">
        <div className="title text-center">
          <h2>Educational Capabilities Overview.</h2>
        </div>
        <div className="roles-table my-4">
          <table className="table">
            <thead>
              <th>University name</th>
              <th>Course name</th>
              <th>Course sector</th>
              <th>Educational source</th>
              <th>Educational capability </th>
            </thead>
            <tbody>
              {eduCapabilities?.map((item, i) => {
                return (
                  <tr key={i}>
                    <td>{item?.universityName}</td>
                    <td>{item?.courseName}</td>
                    <td>{item?.courseSector}</td>
                    <td>{item?.educationalSource}</td>
                    <td></td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
        <FormGroup row className="justify-content-between">
          <Button type={'back'} onClick={() => history.push('educational-capabilities')}>
            BACK
          </Button>
          <div>
            <Button className="mx-3" onClick={() => history.push('educational-capabilities')}>
              Add another
            </Button>
          </div>
          <div>
            <a href="#" className="skip-btn">
              SKIP
            </a>
            <Button type={'next'} onClick={() => history.push('focus-sector-2-1')}>
              NEXT
            </Button>
          </div>
        </FormGroup>
      </Container>
    </React.Fragment>
  );
};

export const EducationalCapabilitiesOverview = () => {
  return (
    <EducationalCapabilitiesContextProvider>
      <EducationalCapabilitiesOverviewPage />
    </EducationalCapabilitiesContextProvider>
  );
};
