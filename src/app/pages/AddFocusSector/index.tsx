/** @jsxImportSource @emotion/react */
import { AvField, AvForm } from 'availity-reactstrap-validation';
import { Button } from 'app/components/Form/Button';
import { FormWrapper } from 'app/components/FormWrapper';
import { Header } from 'app/components/Header';
import {
  AddFocusSectorContext,
  AddFocusSectorContextProvider,
} from 'app/context/AddFocusSectorContext';
import { Multiselect } from 'multiselect-react-dropdown';
import * as React from 'react';
import { Col, Container, FormGroup, Label } from 'reactstrap';
import history from '../../history';
import './index.scss';

const AddFocusSectorPage = () => {
  const [step] = React.useState(1);
  const { addFocusSector, naceCodes } = React.useContext<any>(AddFocusSectorContext);
  const region = JSON.parse(localStorage.getItem('region') || JSON.stringify({}));
  const initialState = {
    ...region,
    naceCode: {},
    ncompanies: 0,
    nemployee: 0,
    nwasteGenerated: 0,
    nwasteIncinerated: 0,
    nwasteLandfilled: 0,
    nwasteRecycled: 0,
    turnover: 0,
    grossValue: 0,
    sectorId: 0,
    sectorSource: '',
  };
  const [inputs, setInputs] = React.useState(initialState);
  const handleSubmit = async () => {
    const success = await addFocusSector(inputs);
    if (success) {
      history.push('focus-sectors');
    }
  };
  const handleInputChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
    const el = e.target as HTMLInputElement;
    setInputs((inputs) => ({ ...inputs, [el.name]: el.value }));
  };
  const onSelect = (selectedList, selectedItem) => {
    delete selectedItem._links;
    setInputs((inputs) => ({ ...inputs, naceCode: selectedItem }));
  };
  return (
    <React.Fragment>
      <Header />
      <Container className="add-focus-sector">
        <div className="title text-center">
          <h2>
            Add Focus Sector -{' '}
            {step === 1 ? 'Role in the Regional Economy' : 'Circularity Potential'}
          </h2>
        </div>
        <AvForm onSubmit={handleSubmit}>
          <FormWrapper>
            <FormGroup row>
              <Label sm={4}>Nace Code</Label>
              <Col sm={8}>
                <Multiselect
                  options={naceCodes}
                  displayValue={'value'}
                  showCheckbox={true}
                  onSelect={onSelect}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Employees in the region</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="nemployee"
                  id="nemployee"
                  required
                  errorMessage=""
                  placeholder="n°"
                  onChange={handleInputChange}
                  value={inputs.nemployee}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter employee region',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Turnover</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="turnover"
                  id="turnover"
                  placeholder="Million €"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={inputs.turnover}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter turnover',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Gross Value Added</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="grossValue"
                  id="grossValue"
                  placeholder="Million €"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={inputs.grossValue}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter grossValue',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Companies in the region</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="ncompanies"
                  id="ncompanies"
                  required
                  errorMessage=""
                  placeholder="n°"
                  onChange={handleInputChange}
                  value={inputs.ncompanies}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter region',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Volume of waste generated</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="nwasteGenerated"
                  id="nwasteGenerated"
                  required
                  errorMessage=""
                  placeholder="tons/year"
                  onChange={handleInputChange}
                  value={inputs.nwasteGenerated}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter volume of wasted generated',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Share of waste recycled</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="nwasteRecycled"
                  id="nwasteRecycled"
                  required
                  errorMessage=""
                  placeholder="%"
                  onChange={handleInputChange}
                  value={inputs.nwasteRecycled}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter volume of wasted recycled',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Share of waste incinerated</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="nwasteIncinerated"
                  id="nwasteIncinerated"
                  required
                  errorMessage=""
                  placeholder="%"
                  onChange={handleInputChange}
                  value={inputs.nwasteIncinerated}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter share of waste incinerated',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Share of waste landfilled</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="nwasteLandfilled"
                  id="nwasteLandfilled"
                  required
                  errorMessage=""
                  placeholder="%"
                  onChange={handleInputChange}
                  value={inputs.nwasteLandfilled}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter share of landfilled',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Source</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="sectorSource"
                  id="sectorSource"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={inputs.sectorSource}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter resources',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row className="justify-content-between">
              <div
                css={{
                  maxWidth: 350,
                  margin: '0 auto',
                  width: '100%',
                  display: 'flex',
                  justifyContent: 'space-between',
                }}
              >
                <Button type={'submit'} onClick={() => console.log('clicked')}>
                  Save
                </Button>
                <Button type={'button'}>Save and add another</Button>
              </div>
            </FormGroup>
            <FormGroup className="d-flex justify-content-between">
              <Button type={'back'} onClick={() => history.push('swot-analysis')}>
                BACK
              </Button>
              <div>
                <a className="skip-btn" onClick={() => history.push('focus-sectors')}>
                  SKIP
                </a>
              </div>
            </FormGroup>
          </FormWrapper>
        </AvForm>
      </Container>
    </React.Fragment>
  );
};

export const AddFocusSector = () => {
  return (
    <AddFocusSectorContextProvider>
      <AddFocusSectorPage />
    </AddFocusSectorContextProvider>
  );
};
