/* eslint-disable jsx-a11y/anchor-is-valid */
import './index.scss';

import * as React from 'react';
import {Container, FormGroup} from 'reactstrap';
import {RndContextProvider,RndContext} from 'app/context/RnDContext';
import {Button} from 'app/components/Form/Button';
import {Header} from 'app/components/Header';
import history from 'app/history';

const RndListPage = () => {
  const {rndList} = React.useContext<any>(RndContext);
  return(
    <React.Fragment>
      <Header/>
      <Container className="companies">
        <div className="title text-center">
          <h2>Provide Research Dev Capabilities</h2>
        </div>
        <div className="roles-table my-4">
          <RndTable data={rndList}/>
        </div>
        <FormGroup row className="justify-content-between">
          <Button type={'back'} onClick={() => history.push('rnd-capabilities')}>BACK</Button>
          <div>
            <a href="#" className="skip-btn">SKIP</a>
            <Button type={'next'} onClick={() => history.push('add-innovation')}>NEXT</Button>
          </div>
        </FormGroup>
      </Container>
    </React.Fragment>
  );
};

export const RndTable = ({data}) => {
  return(
    <table className="table">
    <thead>
      <tr>
      <th>Name</th>
      <th>Department</th>
      <th>IPC Codes</th>
      <th>Number of Researchers </th>
      <th>Number of Startup </th>
      </tr>
    </thead>
    <tbody>
    {data.length > 0 ?
      data.map((rnd) => {
        return(
          <tr>
            <td>{rnd.name}</td>
            <td>{rnd.department}</td>
            <td>{rnd.ipcCode}</td>
            <td>{rnd.nresearchers}</td>
            <td>{rnd.nstartUp}</td>
          </tr>
        );
      })
    :
     null
    }
    </tbody>
  </table>
  );
};

export const RnDList = () => {
  return(
    <RndContextProvider>
      <RndListPage/>
    </RndContextProvider>
  );
};
