import './index.scss';

import * as React from 'react';
import { Header } from 'app/components/Header';
import { Container, FormGroup } from 'reactstrap';
import { Button } from 'app/components/Form/Button';
import {
  AddFocusSectorContextProvider,
  AddFocusSectorContext,
} from 'app/context/AddFocusSectorContext';
import history from 'app/history';

const FocusSectorsPage = () => {
  const { sectors } = React.useContext<any>(AddFocusSectorContext);
  return (
    <React.Fragment>
      <Header />
      <Container className="focus-sector-wrap">
        <div className="title text-center">
          <h2>Identify the role of sectors in regional economy</h2>
        </div>
        <div className="roles-table my-4">
          <FocusTable data={sectors} />
        </div>
        <FormGroup row className="justify-content-between">
          <Button
            type={'back'}
            onClick={() => history.push('add-focus-sector')}
          >
            BACK
          </Button>
          <Button
            type={'button'}
            onClick={() => history.push('add-focus-sector')}
          >
            ADD A SECTOR
          </Button>
          <div>
            <a href="#" className="skip-btn">
              SKIP
            </a>
            <Button type={'next'} onClick={() => history.push('companies')}>
              NEXT
            </Button>
          </div>
        </FormGroup>
      </Container>
    </React.Fragment>
  );
};

export const FocusTable = ({ data }) => {
  console.log(data);
  return (
    <table className="table">
      <thead>
        <tr>
          <th colSpan={5}>Role in the regional economy</th>
          <th colSpan={4}>Circularity potential</th>
          <th colSpan={2}>Source</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>NACE Code</td>
          <td>Employees in the region (n°)</td>
          <td>Turnover (B€)</td>
          <td>Gross Value Added (B€)</td>
          <td>Companies in the region (n°)</td>
          <td>Volume of waste generated</td>
          <td>Share of waste recycled</td>
          <td>Share of waste incinerated</td>
          <td>Share of waste landfilled</td>
          <td>Source</td>
          {/*<td></td>*/}
        </tr>
        {data.map((item, i) => {
          return (
            <tr key={i}>
              <td> {item['_links']['naceCode']['href']}</td>
              <td>{item.nemployee}</td>
              <td>{item.turnover}</td>
              <td>{item.grossValue}</td>
              <td>{item.ncompanies}</td>
              <td>{item.nwasteGenerated}</td>
              <td>{item.nwasteRecycled}</td>
              <td>{item.nwasteIncinerated}</td>
              <td>{item.nwasteLandfilled}</td>
              <td>{item.sectorSource}</td>
              {/*<td><a href="">Edit</a> </td>*/}
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export const FocusSectors = () => {
  return (
    <AddFocusSectorContextProvider>
      <FocusSectorsPage />
    </AddFocusSectorContextProvider>
  );
};
