import { Button } from 'app/components/Form/Button';
import { Header } from 'app/components/Header';
import * as React from 'react';
import { Container, FormGroup } from 'reactstrap';

export const FinalPage = () => {
  return (
    <React.Fragment>
      <Header />
      <Container>
        <div className="title text-center my-5">
          <h1>
            <b>
              Congratulation, you have <br /> completed the mapping of your <br />
              Region!
            </b>
          </h1>
        </div>
        <FormGroup row className="justify-content-center">
          <Button type={'button'}>Go to Dashboard</Button>
        </FormGroup>
      </Container>
    </React.Fragment>
  );
};
