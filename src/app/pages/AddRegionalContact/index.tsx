import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { AvField, AvForm } from 'availity-reactstrap-validation';
import { Button } from 'app/components/Form/Button';
import { Header } from 'app/components/Header';
import { AddRegionContext, AddRegionContextProvider } from 'app/context/AddRegionContactContext';
import * as React from 'react';
import { Col, Container, FormGroup, Label } from 'reactstrap';
import history from '../../history';
import './index.scss';

const AddRegionalContactPage = () => {
  const { addRegionalContact } = React.useContext<any>(AddRegionContext);

  const initialState = { name: '', surname: '', emailAddress: '', phone: '' };
  const [inputs, setInputs] = React.useState([initialState]);

  const handleInputChange = (name, value, i) => {
    const ip = inputs;
    ip[i] = { ...ip[i], [name]: value };
    setInputs(ip);
  };
  const handleSubmit: any = async (_event, errors) => {
    if (errors?.length > 0) {
      return false;
    }
    const success = await addRegionalContact(inputs);
    if (success) {
      setTimeout(() => {
        history.push('s3-details');
      }, 1000);
    }
    return;
  };

  return (
    <React.Fragment>
      <Header />
      <Container>
        <div className="title text-center">
          <h2>Add a Regional Contact Point</h2>
        </div>
        <div className="add-region-contact-form">
          <AvForm onSubmit={handleSubmit}>
            {inputs.map((input, i) => {
              return (
                <div className="my-5" key={`i${i}`}>
                  <FormGroup row>
                    <Label sm={4}>Name</Label>
                    <Col sm={8}>
                      <AvField
                        type="text"
                        className="form-control"
                        name={`name${i}`}
                        id={`name${i}`}
                        required
                        errorMessage="Please enter name"
                        onChange={(e) => handleInputChange('name', e.target.value, i)}
                        value={input.name}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label sm={4}>Surname</Label>
                    <Col sm={8}>
                      <AvField
                        type="text"
                        className="form-control"
                        name={`surname${i}`}
                        id={`surname${i}`}
                        required
                        errorMessage=""
                        value={input.surname}
                        onChange={(e) => handleInputChange('surname', e.target.value, i)}
                        validate={{
                          required: {
                            value: true,
                            errorMessage: 'Please enter surname',
                          },
                        }}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label sm={4}>Mail</Label>
                    <Col sm={8}>
                      <AvField
                        type="email"
                        className="form-control"
                        name={`emailAddress${i}`}
                        id={`emailAddress${i}`}
                        required
                        errorMessage=""
                        value={input.emailAddress}
                        onChange={(e) => handleInputChange('emailAddress', e.target.value, i)}
                        validate={{
                          required: {
                            value: true,
                            errorMessage: 'Please enter email',
                          },
                        }}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label sm={4}>Phone Number</Label>
                    <Col sm={8}>
                      <AvField
                        type="phone"
                        className="form-control"
                        name={`phone${i}`}
                        id={`phone${i}`}
                        required
                        errorMessage=""
                        value={input.phone}
                        onChange={(e) => handleInputChange('phone', e.target.value, i)}
                        validate={{
                          required: {
                            value: true,
                            errorMessage: 'Please enter phone',
                          },
                        }}
                      />
                    </Col>
                  </FormGroup>
                </div>
              );
            })}
            <FormGroup row>
              <Col sm={4} />
              <Col sm={8} className="text-center">
                <FontAwesomeIcon
                  icon={faPlusCircle}
                  onClick={() => setInputs([...inputs, initialState])}
                />
              </Col>
            </FormGroup>
            <FormGroup row className="justify-content-between">
              <Button type={'back'} onClick={() => history.push('regions')}>
                BACK
              </Button>
              <Button type={'submit'} onClick={handleSubmit}>
                NEXT
              </Button>
            </FormGroup>
          </AvForm>
        </div>
      </Container>
    </React.Fragment>
  );
};

export const AddRegionalContact: React.FC<any> = () => {
  return (
    <AddRegionContextProvider>
      <AddRegionalContactPage />
    </AddRegionContextProvider>
  );
};
