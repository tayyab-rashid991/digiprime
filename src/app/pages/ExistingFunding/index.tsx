import * as React from 'react';
import {Header} from 'app/components/Header';
import {Col, Container, FormGroup, Label} from 'reactstrap';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import {FormWrapper} from 'app/components/FormWrapper';
import {Button} from 'app/components/Form/Button';
import {ExistingFundingContextProvider,ExistingFundingContext} from 'app/context/ExistingFundingContext';
import history from 'app/history';

const ExistingFundingPage = () => {
  const {addExistingFund} = React.useContext<any>(ExistingFundingContext);
  const initialState = {
    actionPlan:'',
    financialInstrument:'',
    pillarsName:'',
    regionalInnovationSupport:'',
    regionalRdSupport:'',
    startupPrograms:'',
    voucherIndustry:'',
  };
  const [inputs, setInputs] = React.useState(initialState);
  const handleSubmit = async () => {
    const success = await addExistingFund(inputs);
    if(success){
      history.push('funding-overview');
    }
  };
  const handleInputChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
    const el = e.target as HTMLInputElement;
    setInputs(inputs => ({ ...inputs, [el.name]: el.value }));
  };
  return(
    <React.Fragment>
      <Header/>
      <Container className={'companies'}>
        <div className="title text-center">
          <h2>Existing funding instruments</h2>
        </div>
        <AvForm onSubmit={handleSubmit}>
          <div className="add-company-form">
            <FormWrapper>
              <FormGroup row>
                <Label sm={4}>Pillars Name</Label>
                <Col sm={8}>
                  <AvField
                    type="text"
                    className="form-control"
                    name="pillarsName"
                    id="pillarsName"
                    required
                    errorMessage=""
                    onChange={handleInputChange}
                    value={inputs.pillarsName}
                    validate={{
                      required: {
                        value: true,
                        errorMessage: 'Please enter pillar name',
                      },
                    }}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label sm={4}>Action Plan</Label>
                <Col sm={8}>
                  <AvField
                    type="text"
                    className="form-control"
                    name="actionPlan"
                    id="actionPlan"
                    required
                    errorMessage=""
                    onChange={handleInputChange}
                    value={inputs.actionPlan}
                    validate={{
                      required: {
                        value: true,
                        errorMessage: 'Please enter action plan',
                      },
                    }}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label sm={4}>Financial Instruments</Label>
                <Col sm={8}>
                  <AvField
                    type="text"
                    className="form-control"
                    name="financialInstrument"
                    id="financialInstrument"
                    required
                    errorMessage=""
                    onChange={handleInputChange}
                    value={inputs.financialInstrument}
                    validate={{
                      required: {
                        value: true,
                        errorMessage: 'Please enter action plan',
                      },
                    }}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label sm={4}>R&D Support Program</Label>
                <Col sm={8}>
                  <AvField
                    type="text"
                    className="form-control"
                    name="regionalRdSupport"
                    id="regionalRdSupport"
                    required
                    errorMessage=""
                    onChange={handleInputChange}
                    value={inputs.regionalRdSupport}
                    validate={{
                      required: {
                        value: true,
                        errorMessage: 'Please enter regional r&d support',
                      },
                    }}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label sm={4}>Regional Innovation Support</Label>
                <Col sm={8}>
                  <AvField
                    type="text"
                    className="form-control"
                    name="regionalInnovationSupport"
                    id="regionalInnovationSupport"
                    required
                    errorMessage=""
                    onChange={handleInputChange}
                    value={inputs.regionalInnovationSupport}
                    validate={{
                      required: {
                        value: true,
                        errorMessage: 'Please enter regional innovation support',
                      },
                    }}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label sm={4}>Vouchers for Industry</Label>
                <Col sm={8}>
                  <AvField
                    type="text"
                    className="form-control"
                    name="voucherIndustry"
                    id="voucherIndustry"
                    required
                    errorMessage=""
                    onChange={handleInputChange}
                    value={inputs.voucherIndustry}
                    validate={{
                      required: {
                        value: true,
                        errorMessage: 'Please enter industry vouchers',
                      },
                    }}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label sm={4}>Start-up Support Programs</Label>
                <Col sm={8}>
                  <AvField
                    type="text"
                    className="form-control"
                    name="startupPrograms"
                    id="startupPrograms"
                    required
                    errorMessage=""
                    onChange={handleInputChange}
                    value={inputs.startupPrograms}
                    validate={{
                      required: {
                        value: true,
                        errorMessage: 'Please enter startup programs',
                      },
                    }}
                  />
                </Col>
              </FormGroup>
            </FormWrapper>
          </div>
          <FormGroup className="d-flex justify-content-between">
            <Button type={'back'} onClick={() => history.push('')}>BACK</Button>
            <div>
              <Button type={'back'} className="mx-3" onClick={() => handleSubmit}>Save</Button>
              <Button type={'back'} onClick={() => console.log('add another')}>Save and add another</Button>
            </div>
            <Button type={'next'} onClick={() => console.log('add another')}>NEXT</Button>
          </FormGroup>
        </AvForm>
      </Container>
    </React.Fragment>
  );
};

export const ExistingFunding = () => {
  return(
    <ExistingFundingContextProvider>
      <ExistingFundingPage/>
    </ExistingFundingContextProvider>
  );
};
