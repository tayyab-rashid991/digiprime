import * as React from 'react';
import {Header} from 'app/components/Header';
import {ExistingFundingContextProvider,ExistingFundingContext} from 'app/context/ExistingFundingContext';
import {Container, FormGroup} from 'reactstrap';
import {Button} from 'app/components/Form/Button';
import history from 'app/history';

const FundingOverviewPage = () => {
  const {existingInstruments} = React.useContext<any>(ExistingFundingContext);
  return(
    <React.Fragment>
      <Header/>
      <Container className={'companies'}>
        <div className="title text-center">
          <h2>Existing funding instruments overview</h2>
        </div>
        <div className="roles-table my-4">
          <table className="table">
            <thead>
            <tr>
              <th>Pillars</th>
              <th>Action Plan</th>
              <th>Financial Instruments</th>
              <th>Regional R&D Support Programs</th>
              <th>Regional Innovation Support Programs</th>
              <th>Vouchers for Industry</th>
              <th>Start-Up Support Programs</th>
            </tr>
            </thead>
            <tbody>
            {existingInstruments.map((item,i) => {
              return(
                <tr key={i}>
                  <td>{item.pillarsName}</td>
                  <td>{item.actionPlan}</td>
                  <td>{item.financialInstrument}</td>
                  <td>{item.regionalRdSupport}</td>
                  <td>{item.regionalInnovationSupport}</td>
                  <td>{item.voucherIndustry}</td>
                  <td>{item.startupPrograms}</td>
                </tr>
              );
            })}
            </tbody>
          </table>
        </div>
        <FormGroup row className="justify-content-between">
          <Button type={'back'} onClick={() => history.push('existing-funding')}>BACK</Button>
          <Button type={'button'} onClick={() => history.push('existing-funding')}>Add funding instrument</Button>
          <div>
            <Button type={'next'} onClick={() => history.push('complete-mapping')}>NEXT</Button>
          </div>
        </FormGroup>
      </Container>
    </React.Fragment>
  );
};

export const FundingOverview = () => {
  return(
    <ExistingFundingContextProvider>
      <FundingOverviewPage/>
    </ExistingFundingContextProvider>
  );
};
