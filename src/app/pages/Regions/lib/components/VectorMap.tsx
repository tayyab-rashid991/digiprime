import React from 'react';
import './VectorMap.scss';
import PropTypes from 'prop-types';
import maps from './../maps';

const $ = (window as any).jQuery;

class VectorMap extends React.PureComponent {
  constructor(props: any) {
    super(props);

    (this as any).$node = null;
    (this as any).$mapObject = null;
  }
  /**
   * load required map
   */
  componentWillMount() {
    const { map } = this.props as any;

    if (map && maps.indexOf(map) !== -1) {
      require(`./../maps/${map}`);
    } else {
      throw new Error(`No such map, please select one of the following: ${maps.join()}`);
    }
  }

  /**
   * generate the map
   */
  componentDidMount() {
    const { map } = this.props as any;

    (this as any).$node = $(this.refs.map);

    if (map) {
      (this as any).$node.vectorMap({ ...this.props });
      (this as any).$mapObject = (this as any).$node.vectorMap('get', 'mapObject');
    }
  }

  /**
   * re-render map with props change
   */
  componentDidUpdate() {
    const { map } = this.props as any;

    (this as any).$node = $(this.refs.map);
    (this as any).$node.empty(); // remove old one

    if (map) {
      (this as any).$node.vectorMap({ ...this.props });
      (this as any).$mapObject = (this as any).$node.vectorMap('get', 'mapObject');
    }
  }

  /**
   * set map background color
   * @param color
   */
  setBackgroundColor(color) {
    (this as any).$mapObject.setBackgroundColor(color);
  }

  /**
   * get jvector map object
   * @returns {null|*}
   */
  getMapObject() {
    return (this as any).$mapObject;
  }

  render() {
    const props = {} as any;
    const { containerStyle, containerClassName } = this.props as any;

    // append inline style if exists
    if (containerStyle) {
      props.style = containerStyle;
    }

    // append class if exists
    if (containerClassName) {
      props.className = containerClassName;
    }

    return <div ref="map" {...props} />;
  }
}

(VectorMap as any).propTypes = {
  containerStyle: PropTypes.object,
  containerClassName: PropTypes.string,
  map: PropTypes.oneOf(maps).isRequired,
  backgroundColor: PropTypes.string,
  zoomOnScroll: PropTypes.bool,
  zoomOnScrollSpeed: PropTypes.bool,
  panOnDrag: PropTypes.bool,
  zoomMax: PropTypes.number,
  zoomMin: PropTypes.number,
  zoomStep: PropTypes.number,
  zoomAnimate: PropTypes.bool,
  regionsSelectable: PropTypes.bool,
  regionsSelectableOne: PropTypes.bool,
  markersSelectable: PropTypes.bool,
  markersSelectableOne: PropTypes.bool,
  regionStyle: PropTypes.object,
  regionLabelStyle: PropTypes.object,
  markerStyle: PropTypes.object,
  markerLabelStyle: PropTypes.object,
  markers: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  series: PropTypes.object,
  focusOn: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  labels: PropTypes.object,
  selectedRegions: PropTypes.oneOfType([PropTypes.array, PropTypes.object, PropTypes.string]),
  selectedMarkers: PropTypes.oneOfType([PropTypes.array, PropTypes.object, PropTypes.string]),
  onRegionTipShow: PropTypes.func,
  onRegionOver: PropTypes.func,
  onRegionOut: PropTypes.func,
  onRegionClick: PropTypes.func,
  onRegionSelected: PropTypes.func,
  onMarkerTipShow: PropTypes.func,
  onMarkerOver: PropTypes.func,
  onMarkerOut: PropTypes.func,
  onMarkerClick: PropTypes.func,
  onMarkerSelected: PropTypes.func,
  onViewportChange: PropTypes.func,
  zoomButtons: PropTypes.bool,
  style: PropTypes.object,
};

export default VectorMap;
