import { Button } from 'app/components/Form/Button';
import { Header } from 'app/components/Header';
import { RegionsContext, RegionsContextProvider } from 'app/context/RegionsContext';
import { Multiselect } from 'multiselect-react-dropdown';
import * as React from 'react';
import { Col, Container, Row } from 'reactstrap';
import history from '../../history';
import './index.scss';

const RegionsPage = () => {
  const { regions, countries } = React.useContext<any>(RegionsContext);
  const [region, setRegion] = React.useState({});
  const [error, setError] = React.useState(false);
  const [selectedCountry, setCountry] = React.useState({});

  const onRegionSelect = (selectedList, selectedItem) => {
    setRegion({
      regionId: selectedItem.regionId,
      regionName: selectedItem.regionName,
    });
  };
  const onCountrySelect = (selectedList, selectedItem) => {
    setCountry({
      countryCode: selectedItem.countryCode,
      countryId: selectedItem.countryId,
      countryName: selectedItem.countryName,
    });
  };
  const checkRegion = () => {
    console.log(region);
    console.log(selectedCountry);
    if (!Object.keys(region).length && !Object.keys(selectedCountry).length) {
      setError(true);
    } else {
      const obj = {
        region,
      };
      obj.region['country'] = selectedCountry;
      localStorage.setItem('region', JSON.stringify(obj));
      history.push('add-regional-contact');
    }
  };
  return (
    <React.Fragment>
      <Header />
      <Container className="regions">
        <Row>
          <Col sm={8}>
            <h3 className="text-center">Which Region do you represent?</h3>
            <div className="region-select">
              <Multiselect
                options={regions}
                displayValue={'regionName'}
                singleSelect
                onSelect={onRegionSelect}
              />
              {error && Object.keys(region).length === 0 && (
                <span style={{ color: '#dc3545', marginLeft: '70px' }}>Please select region</span>
              )}

              <Multiselect
                options={countries}
                displayValue={'countryName'}
                singleSelect
                onSelect={onCountrySelect}
              />
              {error && Object.keys(selectedCountry).length === 0 && (
                <span style={{ color: '#dc3545', marginLeft: '70px' }}>Please select country</span>
              )}
            </div>
            {/*<RegionMap/>*/}
          </Col>
          <Col sm={4}>
            <div className="setup-steps py-3 px-4">
              <p>
                As a regional representative, you are asked to provide data regarding the following
                topics:
              </p>
              <ul className="list-unstyled">
                <li>
                  <strong>Step 1.1</strong>RIS 3
                </li>
                <li>
                  <strong>Step 1.2</strong>SWOT Analysis
                </li>
                <li>
                  <strong>Step 2.1</strong>Focus Sectors
                </li>
                <li>
                  <strong>Step 2.2</strong>Companies
                </li>
                <li>
                  <strong>Step 3</strong>Capabilities View
                </li>
                <li>
                  <strong>Step 3.1</strong>R&D Capabilities
                </li>
                <li>
                  <strong>Step 3.2</strong>Innovation Capabilities
                </li>
                <li>
                  <strong>Step 3.3</strong>Education Capabilities
                </li>
                <li>
                  <strong>Step 4</strong>Emerging Ideas
                </li>
                <li>
                  <strong>Step 5</strong>Existing Circular Economy Legislation
                </li>
                <li>
                  <strong>Step 6</strong>Existing Funding Instruments
                </li>
              </ul>
              <h4 className="py-4">HOTSPOTS</h4>
              <p>
                The precision and reliability of the output depends on the quantity and reliability
                of data you are going to input
              </p>
            </div>
            <Button className="float-right" type={'next'} onClick={checkRegion}>
              NEXT
            </Button>
          </Col>
        </Row>
      </Container>
    </React.Fragment>
  );
};

export const Regions: React.FC = () => {
  return (
    <RegionsContextProvider>
      <RegionsPage />
    </RegionsContextProvider>
  );
};
