/** @jsxImportSource @emotion/react */
import history from 'app/history';
import { AvField, AvForm } from 'availity-reactstrap-validation';
import { Button } from 'app/components/Form/Button';
import { NaceCodeSelect } from 'app/components/Form/NaceCodeDropdown';
import { Header } from 'app/components/Header';
import { AddCompanyContext, AddCompanyProvider } from 'app/context/AddCompanyContext';
import { Multiselect } from 'multiselect-react-dropdown';
import * as React from 'react';
import { Col, Container, FormGroup, Label } from 'reactstrap';
import './index.scss';

const AddCompany = () => {
  const [positions] = React.useState(1);
  const [input] = React.useState(1);
  const [output] = React.useState(1);
  const { pChain, appDomain, addCompany } = React.useContext<any>(AddCompanyContext);
  const initialState = {
    companyId: 0,
    nameCompany: '',
    technologicalCap: '',
    inputDescr: '',
    outputDescr: '',
    notUsedDescr: null,
    inputQty: null,
    outputQty: null,
    notUsedQty: null,
    companySource: '',
    naceCode: {},
  };
  const [inputs, setInputs] = React.useState(initialState);

  const handleInputChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
    const el = e.target as HTMLInputElement;
    setInputs((inputs) => ({ ...inputs, [el.name]: el.value }));
  };
  const handleSubmit = async () => {
    const success = await addCompany(inputs);
    if (success) {
      history.push('/companies');
    }
  };
  const onSelect = (selectedList, selectedItem, name) => {
    delete selectedItem._links;
    setInputs((inputs) => ({ ...inputs, [name]: selectedItem }));
  };
  const TypeOptions = [
    {
      typeDesc: 'Product',
      typeWasteId: 1,
    },
    {
      typeDesc: 'Component',
      typeWasteId: 2,
    },
    {
      typeDesc: 'Material',
      typeWasteId: 3,
    },
    {
      typeDesc: 'Overall',
      typeWasteId: 4,
    },
  ];
  return (
    <React.Fragment>
      <Header />
      <Container>
        <div className="title text-center">
          <h2>Add a Company</h2>
        </div>
        <AvForm onSubmit={handleSubmit}>
          <div className="add-company-form">
            <FormGroup row>
              <Label sm={4}>Company Name</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="nameCompany"
                  id="nameCompany"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={inputs.nameCompany}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter company name',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>
                Nace Code
                <br /> <span>identification number</span>
              </Label>
              <Col sm={8}>
                <NaceCodeSelect
                  onSelect={(selectedList, selectedItem) =>
                    onSelect(selectedList, selectedItem, 'naceCode')
                  }
                />
              </Col>
            </FormGroup>
            <div className="group-fields">
              {Array.from(Array(positions), (e, i) => {
                return (
                  <div key={i}>
                    <FormGroup row>
                      <Label sm={4}>Position in the circular value-chain</Label>
                      <Col sm={8}>
                        <Multiselect
                          options={pChain}
                          selectionLimit="3"
                          placeholder="Select up to 3"
                          displayValue="positionValueChainDescr"
                          showCheckbox={true}
                          onSelect={(selectedList, selectedItem) =>
                            onSelect(selectedList, selectedItem, `positionValueChain${i + 1}`)
                          }
                        />
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Label sm={4}>Application Domain of Circular Economy activities</Label>
                      <Col sm={8}>
                        <Multiselect
                          options={appDomain}
                          placeholder="Select up to 3"
                          selectionLimit="3"
                          displayValue="applicationDomainDescr"
                          showCheckbox={true}
                          onSelect={(selectedList, selectedItem) =>
                            onSelect(selectedList, selectedItem, `applicationDomain${i + 1}`)
                          }
                        />
                      </Col>
                    </FormGroup>
                  </div>
                );
              })}
              {/* <FontAwesomeIcon icon={faPlusCircle} onClick={() => setPositions(positions+1)}/> */}
            </div>
            <FormGroup row>
              <Label sm={4}>Technological capabilities, to be indicated through IPC codes</Label>
              <Col sm={8}>
                <a
                  css={{ fontSize: '12px', color: '#419539', fontWeight: '500' }}
                  href="https://www.wipo.int/classifications/ipc/ipcpub/?notion=scheme&version=20210101&symbol=none&menulang=en&lang=en&viewmode=f&fipcpc=no&showdeleted=yes&indexes=no&headings=yes&notes=yes&direction=o2n&initial=A&cwid=none&tree=no&searchmode=smart"
                  target="_blank"
                >
                  Click here to choose IPC Codes
                </a>
                <AvField
                  type="text"
                  className="form-control"
                  name="technologicalCap"
                  id="technologicalCap"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={inputs.technologicalCap}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter technological capabilities',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <div className="group-fields">
              {Array.from(Array(input), (e, i) => {
                return (
                  <div key={i}>
                    <FormGroup row className="h-70">
                      <Label sm={4}>{i === 0 ? 'Input Materials' : ''}</Label>
                      <Col sm={4} className="h-70">
                        <Multiselect
                          options={TypeOptions}
                          singleSelect
                          displayValue="typeDesc"
                          onSelect={(selectedList, selectedItem) =>
                            onSelect(selectedList, selectedItem, 'inputTypeWaste')
                          }
                        />
                      </Col>
                      <Col sm={4} className="h-70">
                        <AvField
                          type="text"
                          className="form-control"
                          name="inputQty"
                          id="inputQty"
                          placeHolder="Quantity (t/year)"
                          onChange={handleInputChange}
                          value={inputs.inputQty}
                        />
                      </Col>
                      <Col sm={4}></Col>
                      <Col sm={8} className="mt-3">
                        <AvField
                          type="text"
                          className="form-control"
                          name="inputDescr"
                          id="inputDescr"
                          placeHolder="Description (use max 5 keywords separated by a comma)"
                          onChange={handleInputChange}
                          value={inputs.inputDescr}
                        />
                      </Col>
                    </FormGroup>
                  </div>
                );
              })}
              {/* <FontAwesomeIcon icon={faPlusCircle} onClick={() => setInput(input + 1)}/> */}
            </div>
            <div className="group-fields">
              {Array.from(Array(output), (e, i) => {
                return (
                  <div key={i}>
                    <FormGroup row className="h-70">
                      <Label sm={4}>{i === 0 ? 'Output Materials' : ''}</Label>
                      <Col sm={4} className="h-70">
                        <Multiselect
                          options={TypeOptions}
                          singleSelect
                          displayValue="typeDesc"
                          onSelect={(selectedList, selectedItem) =>
                            onSelect(selectedList, selectedItem, 'outputTypeWaste')
                          }
                        />
                      </Col>
                      <Col sm={4} className="h-70">
                        <AvField
                          type="text"
                          className="form-control"
                          name="outputQty"
                          id="outputQty"
                          placeHolder="Quantity (t/year)"
                          onChange={handleInputChange}
                          value={inputs.outputQty}
                        />
                      </Col>
                      <Col sm={4}></Col>
                      <Col sm={8} className="mt-3">
                        <AvField
                          type="text"
                          className="form-control"
                          name="outputDescr"
                          id="outputDescr"
                          placeHolder="Description (use max 5 keywords separated by a comma)"
                          onChange={handleInputChange}
                          value={inputs.outputDescr}
                        />
                      </Col>
                    </FormGroup>
                  </div>
                );
              })}
              {/* <FontAwesomeIcon icon={faPlusCircle} onClick={() => setOutput(output +1)}/> */}
            </div>
            <div className="group-fields">
              {Array.from(Array(input), (e, i) => {
                return (
                  <div key={i}>
                    <FormGroup row className="h-70">
                      <Label sm={4}>{i === 0 ? 'Not used Materials' : ''}</Label>
                      <Col sm={4} className="h-70">
                        <Multiselect
                          options={TypeOptions}
                          singleSelect
                          displayValue="typeDesc"
                          onSelect={(selectedList, selectedItem) =>
                            onSelect(selectedList, selectedItem, 'notUsedTypeWaste')
                          }
                        />
                      </Col>
                      <Col sm={4} className="h-70">
                        <AvField
                          type="text"
                          className="form-control"
                          name="notUsedQty"
                          id="notUsedQty"
                          placeHolder="Quantity (t/year)"
                          onChange={handleInputChange}
                          value={inputs.notUsedQty}
                        />
                      </Col>
                      <Col sm={4}></Col>
                      <Col sm={8} className="mt-3">
                        <AvField
                          type="text"
                          className="form-control"
                          name="notUsedDescr"
                          id="notUsedDescr"
                          placeHolder="Description (use max 5 keywords separated by a comma)"
                          onChange={handleInputChange}
                          value={inputs.notUsedDescr}
                        />
                      </Col>
                    </FormGroup>
                  </div>
                );
              })}
              {/* <FontAwesomeIcon icon={faPlusCircle} onClick={() => setInput(input + 1)}/> */}
            </div>
            <FormGroup className="d-flex justify-content-end">
              <Button type={'submit'} onClick={() => handleSubmit}>
                SAVE
              </Button>
            </FormGroup>
          </div>
        </AvForm>
      </Container>
    </React.Fragment>
  );
};

export const AddCompanyPage = () => {
  return (
    <AddCompanyProvider>
      <AddCompany />
    </AddCompanyProvider>
  );
};
