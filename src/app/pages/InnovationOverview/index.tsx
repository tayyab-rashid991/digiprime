import history from 'app/history';
import { Button } from 'app/components/Form/Button';
import { Header } from 'app/components/Header';
import { InnovationContext, InnovationContextProvider } from 'app/context/InnovationContext';
import * as React from 'react';
import { Container, FormGroup } from 'reactstrap';

const InnovationPage = () => {
  const { innovation } = React.useContext<any>(InnovationContext);
  return (
    <React.Fragment>
      <Header />
      <Container className={'companies'}>
        <div className="title text-center">
          <h2>
            Create a circular economy related overview of R&D, innovation and education capabilities
          </h2>
        </div>
        <div className="roles-table my-4">
          <table className="table">
            <thead>
              <tr>
                <th>Name of existing facility (Pilot Plants)</th>
                <th>Application domain of the existing demo centre/pilot line</th>
                <th>List of enabling and available technology & Machinery</th>
                <th>Owner </th>
                <th>List of enabling and available technology & Machinery</th>
                <th>Supported activities/services</th>
                <th>Target Sectors(s)</th>
              </tr>
            </thead>
            <tbody>
              {innovation &&
                innovation?.map((item) => {
                  return (
                    <tr>
                      <td>{item?.facilityName}</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        </div>
        <FormGroup row className="justify-content-between">
          <Button type={'back'} onClick={() => history.push('add-innovation')}>
            BACK
          </Button>
          <Button type={'button'} onClick={() => history.push('add-innovation')}>
            Add a capability
          </Button>
          <div>
            <Button type={'next'} onClick={() => history.push('complete-mapping')}>
              NEXT
            </Button>
          </div>
        </FormGroup>
      </Container>
    </React.Fragment>
  );
};

export const InnovationOverview = () => {
  return (
    <InnovationContextProvider>
      <InnovationPage />
    </InnovationContextProvider>
  );
};
