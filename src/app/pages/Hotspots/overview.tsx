import { Header } from 'app/components/Header';
import { HotspotContext, HotspotContextProvider } from 'app/context/HotspotsContext';
import * as React from 'react';
import { Container } from 'reactstrap';

const HotspotOverviewPage = () => {
  const { hotspots } = React.useContext<any>(HotspotContext);

  return (
    <React.Fragment>
      <Header />
      <Container className="companies">
        <div className="title text-center">
          <h2>Hotspot overview</h2>
        </div>
        <div className="roles-table my-4">
          <table className="table">
            <thead>
              <tr>
                <th>Hotspot description</th>
                <th>Volume (tons/year)</th>
                <th>NACE code</th>
                <th>IPC code </th>
                <th>Application domain</th>
              </tr>
            </thead>
            <tbody>
              {hotspots &&
                hotspots?.map((item, i) => {
                  return (
                    <tr>
                      <td>{item.hotspotDescr}</td>
                      <td>{item.volume}</td>
                      <td>{item.hotspotDescr}</td>
                      <td>{item.ipcCode}</td>
                      <td>{item.hotspotDescr}</td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        </div>
      </Container>
    </React.Fragment>
  );
};

export const HotspotOverview = () => {
  return (
    <HotspotContextProvider>
      <HotspotOverviewPage />
    </HotspotContextProvider>
  );
};
