/** @jsxImportSource @emotion/react */
import { Header } from 'app/components/Header';
import { HotspotContext, HotspotContextProvider } from 'app/context/HotspotsContext';
import history from 'app/utils/history';
import { AvField, AvForm } from 'availity-reactstrap-validation';
import { ApplicationDomainSelect } from 'app/components/Form/ApplicationDomain';
import { Button } from 'app/components/Form/Button';
import { NaceCodeSelect } from 'app/components/Form/NaceCodeDropdown';
import * as React from 'react';
import { Col, Container, FormGroup, Label } from 'reactstrap';

const HotspotPage = () => {
  const { addHotspot } = React.useContext<any>(HotspotContext);
  const initialState = {
    hotspotDescr: '',
    ipcCode: '',
    volume: '',
  };
  const [inputs, setInputs] = React.useState<any>(initialState);
  const handleSubmit = async () => {
    const success = await addHotspot(inputs);
    if (success) {
      history.push('hotspot-overview');
    }
  };
  const handleInputChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
    const el = e.target as HTMLInputElement;
    setInputs((inputs) => ({ ...inputs, [el.name]: el.value }));
  };
  const onSelect = (selectedList, selectedItem, name) => {
    delete selectedItem._links;
    setInputs((inputs) => ({ ...inputs, [name]: selectedItem }));
  };
  return (
    <React.Fragment>
      <Header />
      <div className="title text-center">
        <h2>Hotspots</h2>
      </div>
      <AvForm onSubmit={handleSubmit}>
        <Container>
          <div className="add-company-form">
            <FormGroup row>
              <Label sm={4}>Hotspot description (free short text)</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="hotspotKeyDescr"
                  id="hotspotKeyDescr"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={inputs.hotspotKeyDescr}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter hotspot description',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Hotspot description</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="hotspotDescr"
                  id="hotspotDescr"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={inputs.hotspotDescr}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter hotspot description',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Volume</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="volume"
                  id="volume"
                  placeholder="tons/year"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={inputs.volume}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter volume',
                    },
                  }}
                />
              </Col>
            </FormGroup>

            <FormGroup row>
              <Label sm={4}>
                NACE code(s) of sectors to be involved
                <br />{' '}
              </Label>
              <Col sm={8}>
                <NaceCodeSelect
                  onSelect={(selectedList, selectedItem) =>
                    onSelect(selectedList, selectedItem, 'naceCode1')
                  }
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>
                NACE code(s) of sectors to be involved
                <br />{' '}
              </Label>
              <Col sm={8}>
                <NaceCodeSelect
                  onSelect={(selectedList, selectedItem) =>
                    onSelect(selectedList, selectedItem, 'naceCode2')
                  }
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>
                NACE code(s) of sectors to be involved
                <br />{' '}
              </Label>
              <Col sm={8}>
                <NaceCodeSelect
                  onSelect={(selectedList, selectedItem) =>
                    onSelect(selectedList, selectedItem, 'naceCode3')
                  }
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>IPC Codes</Label>
              <Col sm={8}>
                <a
                  css={{ fontSize: '12px', color: '#419539', fontWeight: '500' }}
                  href="https://www.wipo.int/classifications/ipc/ipcpub/?notion=scheme&version=20210101&symbol=none&menulang=en&lang=en&viewmode=f&fipcpc=no&showdeleted=yes&indexes=no&headings=yes&notes=yes&direction=o2n&initial=A&cwid=none&tree=no&searchmode=smart"
                  target="_blank"
                >
                  Click here to select IPC Codes
                </a>
                <AvField
                  type="text"
                  className="form-control"
                  name="ipcCode"
                  id="ipcCode"
                  required
                  onChange={handleInputChange}
                  value={inputs.ipcCode}
                  errorMessage=""
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter IPC codes',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Position in the secondary flow of the circular value chain</Label>
              <Col sm={8}>
                <ApplicationDomainSelect
                  onSelect={(selectedList, selectedItem) =>
                    onSelect(selectedList, selectedItem, 'applicationDomain')
                  }
                />
              </Col>
            </FormGroup>
          </div>
          <FormGroup className="d-flex justify-content-between">
            <Button type={'back'} onClick={() => handleSubmit}>
              BACK
            </Button>
            <div>
              <Button type={'back'} className="mx-3" onClick={() => handleSubmit}>
                Save
              </Button>
              <Button type={'back'} onClick={() => console.log('add another')}>
                Save and add another
              </Button>
            </div>
            <Button type={'next'} onClick={() => history.push('hotspot-overview')}>
              NEXT
            </Button>
          </FormGroup>
        </Container>
      </AvForm>
    </React.Fragment>
  );
};

export const Hotspot = () => {
  return (
    <HotspotContextProvider>
      <HotspotPage />
    </HotspotContextProvider>
  );
};
