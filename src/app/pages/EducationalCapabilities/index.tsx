import history from 'app/history';
import { AvField, AvForm } from 'availity-reactstrap-validation';
import { Button } from 'app/components/Form/Button';
import { Header } from 'app/components/Header';
import {
  EducationalCapabilitiesContext,
  EducationalCapabilitiesContextProvider,
} from 'app/context/EducationalCapabilitiesContext';
import { Multiselect } from 'multiselect-react-dropdown';
import * as React from 'react';
import { Col, Container, FormGroup, Label } from 'reactstrap';

const EducationalCapabilitiesPage = () => {
  const { courses, addEducationalCapability } = React.useContext<any>(
    EducationalCapabilitiesContext,
  );
  const initialState = {
    courseName: '',
    courseSector: '',
    universityName: '',
    educationalCapabilityId: 0,
    educationalSource: '',
    nStudents: 0,
  };
  const [inputs, setInputs] = React.useState(initialState);
  const handleSubmit = async () => {
    const selectedRegion = JSON.parse(localStorage.getItem('region') || JSON.stringify({}));
    const obj = { ...inputs, region: selectedRegion['region'] };
    const success = await addEducationalCapability(obj);
    if (success) {
      console.log('successfully added ');
      history.push('educational-capabilities-overview');
    } else {
      console.log('error ');
    }
  };
  const onSelect = (selectedList, selectedItem, name) => {
    delete selectedItem._links;
    setInputs((inputs) => ({ ...inputs, [name]: selectedItem }));
  };
  const handleInputChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
    const el = e.target as HTMLInputElement;
    setInputs((inputs) => ({ ...inputs, [el.name]: el.value }));
  };
  return (
    <React.Fragment>
      <Header />
      <AvForm onSubmit={handleSubmit}>
        <Container>
          <div className="title text-center">
            <h2>Add Educational Capabilities</h2>
          </div>
          <div className="add-company-form">
            <FormGroup row>
              <Label sm={4}>Name of University</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="universityName"
                  id="universityName"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={inputs.universityName}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter university name',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Name of Courses</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="courseName"
                  id="courseName"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={inputs.courseName}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter courses name',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Area/Sectors of the courses</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="courseSector"
                  id="courseSector"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={inputs.courseSector}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter Area/Sectors of the courses',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Type of Course</Label>
              <Col sm={8}>
                <Multiselect
                  options={courses}
                  displayValue="courseDescr"
                  showCheckbox={true}
                  onSelect={(selectedList, selectedItem) =>
                    onSelect(selectedList, selectedItem, 'courseType')
                  }
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Number of students per year</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="nStudents"
                  id="nStudents"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={inputs.nStudents}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter Area/Sectors of the courses',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Source</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="educationalSource"
                  id="educationalSource"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={inputs.educationalSource}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter Area/Sectors of the courses',
                    },
                  }}
                />
              </Col>
            </FormGroup>
          </div>
          <FormGroup row className="justify-content-between">
            <Button type={'back'} onClick={() => history.push('capabilities')}>
              BACK
            </Button>
            <div>
              <Button type={'button'} className="mr-3" onClick={() => console.log('clicked')}>
                Save and add another
              </Button>
              <Button type={'submit'} onClick={() => handleSubmit}>
                SAVE
              </Button>
            </div>
          </FormGroup>
        </Container>
      </AvForm>
    </React.Fragment>
  );
};

export const EducationalCapabilities = () => {
  return (
    <EducationalCapabilitiesContextProvider>
      <EducationalCapabilitiesPage />
    </EducationalCapabilitiesContextProvider>
  );
};
