import history from 'app/history';
import { AvField, AvForm } from 'availity-reactstrap-validation';
import { Button } from 'app/components/Form/Button';
import { Header } from 'app/components/Header';
import {
  SmartSpecializationContext,
  SmartSpecializationProvider,
} from 'app/context/SmartSpecializationContext';
import { Multiselect } from 'multiselect-react-dropdown';
import * as React from 'react';
import { Col, Container, FormGroup, Label } from 'reactstrap';
import './index.scss';

const DatePicker = require('reactstrap-date-picker');

const AddSmartSpecializationPage = () => {
  const [date, setDate] = React.useState(new Date().toISOString());
  const initialState = {
    smartSpecializationDescr: '',
    source: '',
    dateOfSource: date,
    economicsDomainId: [],
    policyObjectivesId: [],
    scientificsDomainId: [],
    smartSpecializationId: 0,
  };
  const [inputs, setInputs] = React.useState(initialState);
  const { addSmartSpecialization, economicDomain, scientificsDomains, policyObjectives } =
    React.useContext<any>(SmartSpecializationContext);
  const handleInputChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
    const el = e.target as HTMLInputElement;
    setInputs((inputs) => ({ ...inputs, [el.name]: el.value }));
  };
  const onSelect = (selectedList, selectedItem) => {
    delete selectedItem._links;
    const name = Object.keys(selectedItem)[0];
    setInputs((inputs) => ({ ...inputs, [name]: [...inputs[name], selectedItem] }));
  };
  const onRemove = (selectedList, selectedItem) => {
    const name = Object.keys(selectedItem)[0];
    setInputs((inputs) => ({
      ...inputs,
      [name]: inputs[name].filter((item) => item !== selectedItem),
    }));
  };

  const handleSubmit: any = async (e, errors, val) => {
    if (errors.length > 0) {
      return false;
    }
    const success = await addSmartSpecialization(inputs);
    if (success) {
      setTimeout(() => {
        history.push('s3-details');
      }, 1000);
    }
    return;
  };
  return (
    <React.Fragment>
      <Header />
      <div className="title text-center">
        <h2>Add a Smart Specialization</h2>
      </div>
      <AvForm onSubmit={handleSubmit}>
        <Container className="add-s2">
          <div className="form-holder">
            <FormGroup row>
              <Label sm={4}>Description</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="smartSpecializationDescr"
                  id="smartSpecializationDescr"
                  required
                  errorMessage=""
                  onChange={handleInputChange}
                  value={inputs.smartSpecializationDescr}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter description',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Economic Domains</Label>
              <Col sm={8}>
                <Multiselect
                  id={'ed'}
                  options={economicDomain}
                  displayValue={'codeDomain'}
                  showCheckbox={true}
                  onSelect={onSelect}
                  onRemove={onRemove}
                  closeIcon={'circle'}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Scientific Domains</Label>
              <Col sm={8}>
                <Multiselect
                  options={scientificsDomains}
                  displayValue={'codeDomain'}
                  showCheckbox={true}
                  onSelect={onSelect}
                  onRemove={onRemove}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Policy Objectives</Label>
              <Col sm={8}>
                <Multiselect
                  options={policyObjectives}
                  displayValue="codeDomain"
                  showCheckbox={true}
                  onSelect={onSelect}
                  onRemove={onRemove}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Source</Label>
              <Col sm={8}>
                <AvField
                  type="text"
                  className="form-control"
                  name="source"
                  id="source"
                  required
                  errorMessage=""
                  value={inputs.source}
                  onChange={handleInputChange}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: 'Please enter source',
                    },
                  }}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Date of Source</Label>
              <Col sm={3}>
                <DatePicker id="example-datepicker" value={date} onChange={(v) => setDate(v)} />
              </Col>
            </FormGroup>
            <FormGroup className="d-flex justify-content-between">
              <Button type={'back'} onClick={() => history.push('s3-details')}>
                BACK{' '}
              </Button>
              <div className="justify-content-end">
                <Button type={'submit'} className="mr-3" onClick={handleSubmit}>
                  Save and Next{' '}
                </Button>
                <Button
                  type={'button'}
                  onClick={((e, errors) => handleSubmit(e, errors, 'another')) as any}
                >
                  Save and add another
                </Button>
              </div>
            </FormGroup>
          </div>
        </Container>
      </AvForm>
    </React.Fragment>
  );
};

export const AddSmartSpecialization = () => {
  return (
    <SmartSpecializationProvider>
      <AddSmartSpecializationPage />
    </SmartSpecializationProvider>
  );
};
