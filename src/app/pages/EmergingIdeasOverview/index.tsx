/** @jsxImportSource @emotion/react */
import * as React from 'react';
import { Header } from 'app/components/Header';
import { Container, FormGroup } from 'reactstrap';
import { Button } from 'app/components/Form/Button';
import history from 'app/history';
import { EmergingIdeasContextProvider, EmergingIdeasContext } from 'app/context/EmergingIdeasContext';

const EmergingIdeasOverviewPage = () => {
  const { ideas } = React.useContext<any>(EmergingIdeasContext);
  return (
    <React.Fragment>
      <Header />
      <Container className={'companies'}>
        <div className="title text-center">
          <h2>Emerging Ideas Overview</h2>
        </div>
        <div className="roles-table my-4">
          <table className="table">
            <thead>
              <tr>
                <th>Proposer</th>
                <th>Abstract</th>
                <th>Potential partnership</th>
                <th>Position in the value-chain </th>
                <th>Application domain</th>
                <th>Target product/material/service</th>
                <th colSpan={3} style={{ background: '#7ECD76', textAlign: 'center' }}>
                  Expected impact
                </th>
              </tr>
            </thead>
            <thead>
              <tr>
                <td colSpan={6}></td>
                <td>Social</td>
                <td>Economic</td>
                <td>Environmental</td>
              </tr>
            </thead>
            <tbody>
              {ideas?.map((idea, i) => {
                return (
                  <tr>
                    <td>{idea.proposerName}</td>
                    <td></td>
                    <td>{idea.potentialPartnership}</td>
                    <td></td>
                    <td></td>
                    <td>{idea.targetType}</td>
                    <td colSpan={3}></td>
                  </tr>
                );
              })}
              <tr></tr>
            </tbody>
          </table>
        </div>
        <FormGroup row className="justify-content-between">
          <Button type={'back'} onClick={() => history.push('emerging-ideas')}>
            BACK
          </Button>
          <Button type={'button'} onClick={() => history.push('emerging-ideas')}>
            Add emerging idea
          </Button>
          <div>
            <Button type={'next'} onClick={() => history.push('add-exist-cir-eco-leg')}>
              NEXT
            </Button>
          </div>
        </FormGroup>
      </Container>
    </React.Fragment>
  );
};

export const EmergingIdeasOverview = () => {
  return (
    <EmergingIdeasContextProvider>
      <EmergingIdeasOverviewPage />
    </EmergingIdeasContextProvider>
  );
};
