import './index.scss';

import * as React from 'react';
import { notify } from 'react-notify-toast';
import { Header } from 'app/components/Header';
import { Container, FormGroup } from 'reactstrap';
import { Button } from 'app/components/Form/Button';
import history from 'app/history';
import { faMinusCircle, faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { jsPDF } from 'jspdf';
import html2canvas from 'html2canvas';
import { API_BASE_URL } from 'app/config';

const swotColumns = ['area', 'strength', 'weaknesses', 'opportunity', 'threats'];
const swotRows = ['', 'environmental', 'economic', 'social', 'regulatory'];
const body = {};

export const SWOTAnalysis = () => {
  const printDocument = () => {
    const input = document.getElementById('print-table');
    if (input) {
      html2canvas(input).then((canvas) => {
        const imgWidth = 208;
        const imgHeight = (canvas.height * imgWidth) / canvas.width;
        const imgData = canvas.toDataURL('img/png');
        const pdf = new jsPDF('p', 'mm', 'a4');
        pdf.addImage(imgData, 'PNG', 0, 0, imgWidth, imgHeight);
        pdf.save('download.pdf');
      });
    }
  };

  const addSwots = async () => {
    let region = localStorage.getItem('region') || JSON.stringify({});
    if (region) {
      region = JSON.parse(region)['region'];
    }

    Object.keys(swot).map((s) => {
      body[s] = swot[s]['save'].map((o) => ({ ...o, region }));
    });
    console.log(body);
    const res = await fetch(`${API_BASE_URL}custommethods/addSwots`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: { 'Content-Type': 'text/plain', 'Access-Control-Allow-Origin': '*' },
    });
    if (res.status === 200) {
      notify.show('SWOTs added successfully', 'success');
      history.push('add-focus-sector');
    }
  };

  const [swot, setSwot] = React.useState({
    environmental: {
      save: [],
      inputs: {
        strength: '',
        weaknesses: '',
        opportunity: '',
        threats: '',
      },
    },
    economic: {
      save: [],
      inputs: {
        strength: '',
        weaknesses: '',
        opportunity: '',
        threats: '',
      },
    },
    social: {
      save: [],
      inputs: {
        strength: '',
        weaknesses: '',
        opportunity: '',
        threats: '',
      },
    },
    regulatory: {
      save: [],
      inputs: {
        strength: '',
        weaknesses: '',
        opportunity: '',
        threats: '',
      },
    },
  });

  // handle input change
  const handleInputChange = (row, col, val) => {
    swot[row]['inputs'][col] = val;
  };

  // handle click event of the Remove button
  const handleRemove = (from, index) => {
    swot[from]['save'].splice(index, 1);
    setSwot({ ...swot });
  };

  // handle click event of the Add button
  const handleAdd = (addIn) => {
    const newObj = { ...swot[addIn]['inputs'] };
    swot[addIn]['save'].push(newObj);
    setSwot({ ...swot });
  };
  return (
    <React.Fragment>
      <Header />
      <Container className="swot-analysis">
        <div className="title text-center">
          <h2>Please fill in the SWOT analysis of your Region</h2>
        </div>
        <div className="ris-details my-5" id="print-table">
          <table className="table">
            <thead>
              <tr>
                {swotColumns.map((col, i) => (
                  <th key={`col${i}`} scope="col">
                    {col}
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              <React.Fragment>
                {swotRows.map((area, i) => {
                  return (
                    <React.Fragment>
                      {swot[area] &&
                        swot[area]['save'].map((saveObj, k) => {
                          return (
                            <tr>
                              <td></td>
                              {swotColumns.map((key, index) => {
                                return (
                                  <React.Fragment>
                                    {saveObj[key] !== undefined && (
                                      <td key={`${i}${area}${key}${k}${index}`}>
                                        <div className="swot_wrap">
                                          <div className="detail-field">
                                            <p>{saveObj[key]}</p>
                                            {index + 1 === swotColumns.length && (
                                              <FontAwesomeIcon
                                                icon={faMinusCircle}
                                                onClick={() => handleRemove(area, k)}
                                              />
                                            )}
                                          </div>
                                        </div>
                                      </td>
                                    )}
                                  </React.Fragment>
                                );
                              })}
                            </tr>
                          );
                        })}
                      <tr>
                        {swot[area] && (
                          <td>
                            <label>{area}</label>
                          </td>
                        )}
                        {swot[area] &&
                          Object.keys(swot[area]['inputs']).map((ip, i) => {
                            return (
                              <td>
                                <div className="action">
                                  <input
                                    placeholder="Add more"
                                    onChange={(e) => handleInputChange(area, ip, e.target.value)}
                                  />
                                  {i + 1 === Object.keys(swot[area]['inputs']).length && (
                                    <FontAwesomeIcon
                                      icon={faPlusCircle}
                                      onClick={() => handleAdd(area)}
                                    />
                                  )}
                                </div>
                              </td>
                            );
                          })}
                      </tr>
                    </React.Fragment>
                  );
                })}
              </React.Fragment>
            </tbody>
          </table>
          <FormGroup row className="justify-content-between">
            <Button type={'back'} onClick={() => history.push('s3-details')}>
              BACK
            </Button>
            <Button onClick={printDocument}>Export to PDF</Button>
            <div>
              <a className="skip-btn" onClick={() => history.push('add-focus-sector')}>
                SKIP
              </a>
              <Button type={'next'} onClick={() => addSwots()}>
                NEXT
              </Button>
            </div>
          </FormGroup>
        </div>
      </Container>
    </React.Fragment>
  );
};
