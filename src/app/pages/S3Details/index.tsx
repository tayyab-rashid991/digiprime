import './index.scss';

import * as React from 'react';
import { Header } from 'app/components/Header';
import { Button } from 'app/components/Form/Button';
import { Container, FormGroup } from 'reactstrap';
import history from 'app/history';
import {
  SmartSpecializationProvider,
  SmartSpecializationContext,
} from 'app/context/SmartSpecializationContext';

const S3DetailsPage = ({ props }) => {
  const { smartSpecializationData } = React.useContext<any>(SmartSpecializationContext);
  return (
    <React.Fragment>
      <Header />
      <Container>
        <div className="title text-center">
          <h2>
            Highlight the most strategic innovation areas in the Region in view <br /> of supporting
            the transition to circular economy
          </h2>
        </div>
        <div className="ris-details my-5">
          <SmartSpecializationTable data={smartSpecializationData} />
          <FormGroup row className="justify-content-between">
            <Button type={'back'} onClick={() => history.push('add-regional-contact')}>
              BACK
            </Button>
            <Button type="button" onClick={() => history.push('add-smart-specialization')}>
              ADD S3
            </Button>
            <div>
              <a className="skip-btn" onClick={() => history.push('swot-analysis')}>
                SKIP
              </a>
              <Button type={'next'} onClick={() => history.push('swot-analysis')}>
                NEXT
              </Button>
            </div>
          </FormGroup>
        </div>
      </Container>
    </React.Fragment>
  );
};

export const SmartSpecializationTable = ({ data }) => {
  return (
    <table className="table">
      <thead>
        <tr>
          <th scope="col" style={{ width: '30%' }}>
            Description
          </th>
          <th scope="col">Economic Domains</th>
          <th scope="col">Scientific Domains</th>
          <th scope="col">Policy Objectives</th>
          <th scope="col">Source</th>
          <th scope="col" style={{ width: '130px' }}>
            Date of source
          </th>
          {/*<th scope="col"></th>*/}
        </tr>
      </thead>
      <tbody>
        {data.length < 0 && (
          <tr>
            <td className="empty-record" colSpan={7}>
              <div className="d-flex align-items-center justify-content-center">
                <Button type="button" onClick={() => history.push('add-smart-specialization')}>
                  ADD S3
                </Button>
              </div>
            </td>
          </tr>
        )}
        {data.length > 0 &&
          data?.map((details, i) => {
            return (
              <tr key={i}>
                <td>
                  <p>{details.smartSpecializationDescr}</p>
                </td>
                <td>
                  <p>{details.economicsDomains}</p>
                </td>
                <td>
                  <p>{details.economicsDomains}</p>
                </td>
                <td>
                  <p>{details.policyObjectives}</p>
                </td>
                <td>
                  <p>{details.source}</p>
                </td>
                <td>
                  <p>{details.dateOfSource}</p>
                </td>
                {/*<td><a href="#">EDIT</a> </td>*/}
              </tr>
            );
          })}
      </tbody>
    </table>
  );
};

export const S3Details = (props) => {
  return (
    <SmartSpecializationProvider>
      <S3DetailsPage props={props} />
    </SmartSpecializationProvider>
  );
};
