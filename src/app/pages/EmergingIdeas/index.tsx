/** @jsxImportSource @emotion/react */
import history from 'app/history';
import { AvField, AvForm } from 'availity-reactstrap-validation';
import { ApplicationDomainSelect } from 'app/components/Form/ApplicationDomain';
import { Button } from 'app/components/Form/Button';
import { NaceCodeSelect } from 'app/components/Form/NaceCodeDropdown';
import { FormWrapper } from 'app/components/FormWrapper';
import { Header } from 'app/components/Header';
import { EmergingIdeasContext, EmergingIdeasContextProvider } from 'app/context/EmergingIdeasContext';
import { Multiselect } from 'multiselect-react-dropdown';
import * as React from 'react';
import { Col, Container, FormGroup, Label } from 'reactstrap';

const EmergingIdeasPage = () => {
  const { pChain, impact, postEmergingIdea } = React.useContext<any>(EmergingIdeasContext);
  const initialState = {
    economic: {},
    emergingIdeaId: 0,
    environmental: {},
    positionValueChain: {},
    potentialPartnership: '',
    proposerName: '',
    social: {},
    targetType: '',
  };
  const [inputs, setInputs] = React.useState<any>(initialState);

  const handleSubmit = async () => {
    const obj = { ...inputs, ...JSON.parse(localStorage.getItem('region') || JSON.stringify({})) };
    const success = await postEmergingIdea(obj);
    if (success) {
      history.push('emerging-ideas-overview');
    }
  };
  const handleInputChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
    const el = e.target as HTMLInputElement;
    setInputs((inputs) => ({ ...inputs, [el.name]: el.value }));
  };
  const onSelect = (selectedList, selectedItem, name) => {
    delete selectedItem._links;
    setInputs((inputs) => ({ ...inputs, [name]: selectedItem }));
  };
  return (
    <React.Fragment>
      <Header />
      <Container className="add-focus-sector">
        <div className="title text-center">
          <h2>Emerging ideas</h2>
        </div>
        <AvForm onSubmit={handleSubmit}>
          <div className="add-company-form">
            <FormWrapper>
              <FormGroup row>
                <Label sm={4}>Proposer Name</Label>
                <Col sm={8}>
                  <AvField
                    type="text"
                    className="form-control"
                    name="proposerName"
                    id="proposerName"
                    required
                    errorMessage=""
                    onChange={handleInputChange}
                    value={inputs.proposerName}
                    validate={{
                      required: {
                        value: true,
                        errorMessage: 'Please enter proposer name',
                      },
                    }}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label sm={4}>Abstract</Label>
                <Col sm={8}>
                  <AvField
                    type="text"
                    className="form-control"
                    name="abstract"
                    id="abstract"
                    required
                    errorMessage=""
                    onChange={handleInputChange}
                    value={inputs.abstract}
                    validate={{
                      required: {
                        value: true,
                        errorMessage: 'Please enter abstract',
                      },
                    }}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label sm={4}>Potential Partnership</Label>
                <Col sm={8}>
                  <AvField
                    type="text"
                    className="form-control"
                    name="potentialPartnership"
                    id="potentialPartnership"
                    required
                    errorMessage=""
                    onChange={handleInputChange}
                    value={inputs.potentialPartnership}
                    validate={{
                      required: {
                        value: true,
                        errorMessage: 'Please enter potential partnership',
                      },
                    }}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label sm={4}>Nace Code</Label>
                <Col sm={8}>
                  <NaceCodeSelect
                    onSelect={(selectedList, selectedItem) =>
                      onSelect(selectedList, selectedItem, 'naceCode')
                    }
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label sm={4}>Position in the value-chain of the proposer</Label>
                <Col sm={8}>
                  <Multiselect
                    options={pChain}
                    displayValue="positionValueChainDescr"
                    showCheckbox={true}
                    onSelect={(selectedList, selectedItem) =>
                      onSelect(selectedList, selectedItem, 'positionValueChain')
                    }
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label sm={4}>
                  Position in the secondary flow of the circular value chain of the emerging idea
                </Label>
                <Col sm={8}>
                  <ApplicationDomainSelect
                    onSelect={(selectedList, selectedItem) =>
                      onSelect(selectedList, selectedItem, 'applicationDomain')
                    }
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label sm={4}>Target product/material/service</Label>
                <Col sm={8}>
                  <AvField
                    type="text"
                    className="form-control"
                    name="targetType"
                    id="targetType"
                    required
                    errorMessage=""
                    onChange={handleInputChange}
                    value={inputs.targetType}
                    validate={{
                      required: {
                        value: true,
                        errorMessage: 'Please enter target type',
                      },
                    }}
                  />
                </Col>
              </FormGroup>

              <div className="row">
                <h4 css={{ padding: '30px 0 0 15px' }}>Expected impact</h4>
              </div>
              <hr />
              <FormGroup row>
                <Label sm={4}>Social impact</Label>
                <Col sm={8}>
                  <Multiselect
                    options={impact}
                    displayValue="valueImpactDesc"
                    showCheckbox={true}
                    onSelect={(selectedList, selectedItem) =>
                      onSelect(selectedList, selectedItem, 'social')
                    }
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label sm={4}>Economic impact</Label>
                <Col sm={8}>
                  <Multiselect
                    options={impact}
                    displayValue="valueImpactDesc"
                    showCheckbox={true}
                    onSelect={(selectedList, selectedItem) =>
                      onSelect(selectedList, selectedItem, 'economic')
                    }
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label sm={4}>Environmental impact</Label>
                <Col sm={8}>
                  <Multiselect
                    options={impact}
                    displayValue="valueImpactDesc"
                    showCheckbox={true}
                    onSelect={(selectedList, selectedItem) =>
                      onSelect(selectedList, selectedItem, 'environmental')
                    }
                  />
                </Col>
              </FormGroup>
            </FormWrapper>
          </div>
          <FormGroup className="d-flex justify-content-between">
            <Button type={'back'} onClick={() => history.push('')}>
              BACK
            </Button>
            <div>
              <Button type={'back'} className="mx-3" onClick={() => handleSubmit}>
                Save
              </Button>
              <Button type={'back'} onClick={() => console.log('add another')}>
                Save and add another
              </Button>
            </div>
            <Button type={'next'} onClick={() => console.log('add another')}>
              NEXT
            </Button>
          </FormGroup>
        </AvForm>
      </Container>
    </React.Fragment>
  );
};

export const EmergingIdeas = () => {
  return (
    <EmergingIdeasContextProvider>
      <EmergingIdeasPage />
    </EmergingIdeasContextProvider>
  );
};
