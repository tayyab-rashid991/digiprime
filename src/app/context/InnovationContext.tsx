import * as React from 'react';
import { API_BASE_URL } from 'app/config';

interface IContextProps {
  addInnovation: Function;
  innovation: innovation[];
}

type innovation = {
  innovationCapabilityId: number;
  facilityName: string;
  otherDomainDesc: string;
  owner: string;
};

export const InnovationContext = React.createContext({} as IContextProps);

export const InnovationContextProvider = ({ children }) => {
  const [innovation, setInnovation] = React.useState([]);

  const getInnovationList = async () => {
    const fetchData = await fetch(`${API_BASE_URL}innovationCapabilities`);
    const data = await fetchData.json();
    if (fetchData.status === 200) {
      setInnovation(data['_embedded']['innovationCapabilities']);
    }
  };

  const addInnovation = async (data) => {
    const region: any = JSON.parse(localStorage.getItem('region') || JSON.stringify({}) || JSON.stringify({}));
    const body = data;
    body['region'] = region.region;
    const response = await fetch(`${API_BASE_URL}innovationCapabilities`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(body),
    });

    return response.status === 201;
  };

  React.useEffect(() => {
    getInnovationList();
  }, []);
  const defaultContext: any = {
    addInnovation,
    innovation,
  };
  return <InnovationContext.Provider value={defaultContext}>{children}</InnovationContext.Provider>;
};
