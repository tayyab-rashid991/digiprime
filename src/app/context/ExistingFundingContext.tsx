import * as React from 'react';
import { API_BASE_URL } from 'app/config';

interface IContextProps {
  addExistingFund:Function;
  existingInstruments:[];
}

export const ExistingFundingContext = React.createContext({} as IContextProps);

export const ExistingFundingContextProvider = ({children}) => {
  const [existingInstruments, setExistingInstruments] = React.useState([]);

  const getExistingInstruments = async () => {
    const fetchData = await fetch(`${API_BASE_URL}existingInstrumentses`);
    const jsonData = await fetchData.json();
    if(fetchData.status === 200){
      setExistingInstruments(jsonData['_embedded']['existingInstrumentses']);
    }
  };

  const addExistingFund = async (body) => {
    const response = await fetch(`https:localhost:8443/${API_BASE_URL}/existingInstrumentses`, {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body:JSON.stringify(body),
    });
    if(response.status === 201){
      return true;
    }
    return false;
  };

  React.useEffect(() => {
    getExistingInstruments();
  },              []);
  const defaultContext: any = {
    addExistingFund,
    existingInstruments,
  };
  return(
    <ExistingFundingContext.Provider value={defaultContext}>
      {children}
    </ExistingFundingContext.Provider>
  );
};
