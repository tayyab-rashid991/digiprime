import * as React from 'react';
// import { API_BASE_URL } from 'app/config';

interface IContextProps {
  authenticated: boolean;
}

export const AuthContext = React.createContext({} as IContextProps);

export const AuthContextProvider =  ({ children }) => {

  const defaultContext: any = {
  };
  return (
        <AuthContext.Provider value={defaultContext}>
          {children}
        </AuthContext.Provider>
  );
};
