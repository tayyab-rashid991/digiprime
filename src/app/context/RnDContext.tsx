import * as React from 'react';
import { API_BASE_URL } from 'app/config';

interface IContextProps {
  postResearchCapability: Function;
  rndList: rndList[];
}

type rndList = {
  name: null;
  department: null;
  ipcCode: null;
  nresearchers: 0;
  nstartUp: 0;
};

export const RndContext = React.createContext({} as IContextProps);

export const RndContextProvider = ({ children }) => {
  const [rndList, setRnDList] = React.useState([]);
  const postResearchCapability = async (data) => {
    data['nresearchers'] = parseFloat(data['nresearchers']);
    data['nstartUp'] = parseFloat(data['nstartUp']);
    const response = await fetch(`${API_BASE_URL}researchDevCapabilities`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(data),
    });
    return response.status === 201;
  };
  const fetchRnDList = async () => {
    const fetchData = await fetch(`${API_BASE_URL}researchDevCapabilities`);
    const jsonData = await fetchData.json();
    if (fetchData.status === 200) {
      setRnDList(jsonData['_embedded']['researchDevCapabilities']);
    }
  };
  React.useEffect(() => {
    fetchRnDList();
  }, []);
  const defaultContext: any = {
    postResearchCapability,
    rndList,
  };
  return <RndContext.Provider value={defaultContext}>{children}</RndContext.Provider>;
};
