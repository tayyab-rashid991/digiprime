import * as React from 'react';
import { API_BASE_URL } from 'app/config';

interface IContextProps {
  companies: companies[];
}

type companies = {
  companyId:number;
  companySource: string;
  inputDescr: string;
  inputQty: number;
  nameCompany: string;
  notUsedDescr: number;
  notUsedQty: number;
  outputDescr: string;
  outputQty: number;
  technologicalCap: string;
};

export const CompaniesContext = React.createContext({} as IContextProps);

export const CompaniesProvider = ({children}) => {
  const [companies,setCompanies] = React.useState([]);
  const fetchCompanies = async () => {
    const fetchData = await fetch(`${API_BASE_URL}custommethods/getAllCompanyDetails`);
    const jsonData = await fetchData.json();
    if(fetchData.status === 200){
      setCompanies(jsonData['content']);
    }
  };
  React.useEffect(() => {
    fetchCompanies();
  },              []);
  const defaultContext: any = {
    companies,
  };
  return(
    <CompaniesContext.Provider value={defaultContext}>
      {children}
    </CompaniesContext.Provider>
  );
};
