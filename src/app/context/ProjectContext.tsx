import * as React from 'react';
import { API_BASE_URL } from 'app/config';

interface IContextProps {
  sources: [];
  addProject: Function;
  capabilities: [];
}
export const ProjectContext = React.createContext({} as IContextProps);

export const ProjectContextProvider = ({ children }) => {
  const [sources, setSources] = React.useState([]);
  const [capabilities, setCapabilities] = React.useState([]);

  const addProject = async (body) => {
    const response = await fetch(`https:localhost:8443/${API_BASE_URL}capabilities`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(body),
    });

    return response.status === 201;
  };
  const fetchCapabilities = async () => {
    const fetchData = await fetch(`${API_BASE_URL}capabilities`);
    const jsonData = await fetchData.json();
    if (fetchData.status === 200) {
      console.log(jsonData);
      setCapabilities(jsonData['_embedded']['capabilities']);
    }
  };

  const fetchSources = async () => {
    const fetchData = await fetch(`${API_BASE_URL}sources`);
    const jsonData = await fetchData.json();
    if (fetchData.status === 200) {
      setSources(jsonData['_embedded']['sources']);
    }
  };

  React.useEffect(() => {
    fetchSources();
    fetchCapabilities();
  }, []);
  const defaultContext: any = {
    sources,
    addProject,
    capabilities,
  };
  return <ProjectContext.Provider value={defaultContext}>{children}</ProjectContext.Provider>;
};
