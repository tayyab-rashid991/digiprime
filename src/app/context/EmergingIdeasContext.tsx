import * as React from 'react';
import { API_BASE_URL } from 'app/config';

interface IContextProps {
  pChain: [];
  impact: [];
  ideas: [];
  postEmergingIdea: Function;
}

export const EmergingIdeasContext = React.createContext({} as IContextProps);

export const EmergingIdeasContextProvider = ({ children }) => {
  const [pChain, setPChain] = React.useState([]);
  const [impact, setImpact] = React.useState([]);
  const [ideas, setIdeas] = React.useState([]);

  const postEmergingIdea = async (body) => {
    const response = await fetch(`${API_BASE_URL}emergingIdeas`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(body),
    });

    return response.status === 201;
  };
  const getEmergingIdeas = async () => {
    const fetchData = await fetch(`${API_BASE_URL}emergingIdeas`);
    const jsonData = await fetchData.json();
    if (fetchData.status === 200) {
      setIdeas(jsonData['_embedded']['emergingIdeas']);
    }
  };
  const getPositionChain = async () => {
    const getPositionChainData = await fetch(`${API_BASE_URL}positionValueChains`, {
      method: 'GET',
    });
    if (getPositionChainData.status === 200) {
      const jsonData = await getPositionChainData.json();
      setPChain(jsonData['_embedded']['positionValueChains']);
    }
  };

  const getValueImpact = async () => {
    const fetchData = await fetch(`${API_BASE_URL}valueImpacts`);
    const jsonData = await fetchData.json();
    if (fetchData.status === 200) {
      setImpact(jsonData['_embedded']['valueImpacts']);
    }
  };

  React.useEffect(() => {
    getPositionChain();
    getValueImpact();
    getEmergingIdeas();
  }, []);
  const defaultContext: any = {
    pChain,
    impact,
    postEmergingIdea,
    ideas,
  };
  return (
    <EmergingIdeasContext.Provider value={defaultContext}>{children}</EmergingIdeasContext.Provider>
  );
};
