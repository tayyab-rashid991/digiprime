import * as React from 'react';
import { API_BASE_URL } from 'app/config';

interface IContextProps {
  addHotspot: Function;
  hotspots: [];
}

export const HotspotContext = React.createContext({} as IContextProps);

export const HotspotContextProvider = ({ children }) => {
  const [hotspots, setHotspot] = React.useState([]);

  const getHotspots = async () => {
    const fetchData = await fetch(`${API_BASE_URL}hotspots`);
    const jsonData = await fetchData.json();
    if (fetchData.status === 200) {
      setHotspot(jsonData['_embedded']['hotspots']);
    }
  };

  const addHotspot = async (body) => {
    const obj = { ...body, ...JSON.parse(localStorage.getItem('region') || JSON.stringify({}) || JSON.stringify({})) };
    const response = await fetch(`https:localhost:8443/${API_BASE_URL}hotspots`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(obj),
    });
    if (response.status === 201) {
      return true;
    }
    return false;
  };

  React.useEffect(() => {
    getHotspots();
  }, []);
  const defaultContext: any = {
    addHotspot,
    hotspots,
  };
  return <HotspotContext.Provider value={defaultContext}>{children}</HotspotContext.Provider>;
};
