import * as React from 'react';
import { API_BASE_URL } from 'app/config';

interface IContextProps {
  courses: [];
  addEducationalCapability: Function;
  eduCapabilities: [];
}

export const EducationalCapabilitiesContext = React.createContext({} as IContextProps);

export const EducationalCapabilitiesContextProvider = ({ children }) => {
  const [courses, setCourses] = React.useState();
  const [eduCapabilities, setEduCapabilities] = React.useState([]);
  const fetchCourses = async () => {
    const fetchData = await fetch(`${API_BASE_URL}courseTypes`);
    const jsonData = await fetchData.json();
    if (fetchData.status === 200) {
      setCourses(jsonData['_embedded']['courseTypes']);
    }
  };

  const fetchEducationalCapabilities = async () => {
    const fetchData = await fetch(`${API_BASE_URL}educationalCapabilities`);
    const jsonData = await fetchData.json();
    if (fetchData.status === 200) {
      setEduCapabilities(jsonData['_embedded']['educationalCapabilities']);
    }
  };

  const addEducationalCapability = async (body) => {
    const response = await fetch(`https:localhost:8443/${API_BASE_URL}educationalCapabilities`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(body),
    });

    return response.status === 201;
  };

  React.useEffect(() => {
    fetchCourses();
    fetchEducationalCapabilities();
  }, []);

  const defaultContext: any = {
    courses,
    addEducationalCapability,
    eduCapabilities,
  };
  return (
    <EducationalCapabilitiesContext.Provider value={defaultContext}>
      {children}
    </EducationalCapabilitiesContext.Provider>
  );
};
