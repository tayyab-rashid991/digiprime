import * as React from 'react';
import { API_BASE_URL } from 'app/config';
import { notify } from 'react-notify-toast';

interface IContextProps {
  smartSpecializationData: any;
  setSmartSpecializationData(smartSpecializationData: any): void;
  addSmartSpecialization: Function;
  economicDomain: [];
  setEconomicDomain(economicDomain: []): void;
  scientificsDomains: [];
  setScientificsDomains(scientificsDomains: []): void;
  policyObjectives: [];
  setPolicyObjectives(policyObjectives: []): void;
}

export const SmartSpecializationContext = React.createContext({} as IContextProps);

export const SmartSpecializationProvider = ({ children }) => {
  const [smartSpecializationData, setSmartSpecializationData] = React.useState({});
  const [economicDomain, setEconomicDomain] = React.useState([]);
  const [policyObjectives, setPolicyObjectives] = React.useState([]);
  const [scientificsDomains, setScientificsDomains] = React.useState([]);

  const fetchSmartSpecialization = async () => {
    const fetchSSData = await fetch(`${API_BASE_URL}smartSpecializations`, {
      method: 'GET',
    });
    if (fetchSSData.status === 200) {
      const jsonData = await fetchSSData.json();
      setSmartSpecializationData(jsonData['_embedded']['smartSpecializations']);
    } else {
      notify.show('Error data. Please try again or contact your administrator.', 'error');
    }
  };
  const fetchEconomicDomains = async () => {
    const fetchData = await fetch(`${API_BASE_URL}economicsDomains`);
    if (fetchData.status === 200) {
      const jsonData = await fetchData.json();
      setEconomicDomain(jsonData['_embedded']['economicsDomains']);
    } else {
      notify.show('Error data. Please try again or contact your administrator.', 'error');
    }
  };
  const fetchScientificDomains = async () => {
    const fetchData = await fetch(`${API_BASE_URL}scientificsDomains`);
    if (fetchData.status === 200) {
      const jsonData = await fetchData.json();
      setScientificsDomains(jsonData['_embedded']['scientificsDomains']);
    } else {
      notify.show('Error data. Please try again or contact your administrator.', 'error');
    }
  };
  const fetchPolicyObjectives = async () => {
    const fetchData = await fetch(`${API_BASE_URL}policyObjectiveses`);
    if (fetchData.status === 200) {
      const jsonData = await fetchData.json();
      setPolicyObjectives(jsonData['_embedded']['policyObjectiveses']);
    } else {
      notify.show('Error data. Please try again or contact your administrator.', 'error');
    }
  };
  const addSmartSpecialization: any = async (body) => {
    const response = await fetch(`${API_BASE_URL}smartSpecializations`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: { 'Content-Type': 'application/json' },
    });

    if (response.status === 201) {
      notify.show('Smart specialization added successfully', 'success');
      return true;
    }
    if (response.status === 401) {
      const errorBody = await response.json();
      notify.show(errorBody, 'error');
    }
    return;
  };

  React.useEffect(() => {
    fetchSmartSpecialization();
    fetchEconomicDomains();
    fetchScientificDomains();
    fetchPolicyObjectives();
  }, []);
  const defaultContext: any = {
    smartSpecializationData,
    addSmartSpecialization,
    economicDomain,
    scientificsDomains,
    policyObjectives,
  };
  return (
    <SmartSpecializationContext.Provider value={defaultContext}>
      {children}
    </SmartSpecializationContext.Provider>
  );
};
