import * as React from 'react';
import { notify } from 'react-notify-toast';
import { API_BASE_URL } from 'app/config';

interface IContextProps {
  addRegionalContact: Function;
}

export const AddRegionContext = React.createContext({} as IContextProps);

export const AddRegionContextProvider = ({ children }) => {
  const addRegionalContact = async (info) => {
    const selectedRegion = JSON.parse(localStorage.getItem('region') || JSON.stringify({}));
    const response = await fetch(`${API_BASE_URL}userAddedNotifications`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        emailAddress: info.emailAddress,
        name: info.name,
        phone: parseInt(info.phone, 10),
        surname: info.surname,
        userAddedNotificationId: 0,
        ...selectedRegion,
      }),
    });
    if (response.status === 201) {
      notify.show('Region contact point added successfully', 'success');
      return true;
    } else {
      const errorBody = await response.json();
      notify.show(errorBody, 'error');
      return false;
    }
  };

  const defaultContext: any = {
    addRegionalContact,
  };
  return <AddRegionContext.Provider value={defaultContext}>{children}</AddRegionContext.Provider>;
};
