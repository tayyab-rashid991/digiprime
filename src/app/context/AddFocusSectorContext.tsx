import * as React from 'react';
import { API_BASE_URL } from 'app/config';

interface IContextProps {
  addFocusSector: Function;
  naceCodes: [];
  sectors: [];
  fetchSectors: Function;
}

export const AddFocusSectorContext = React.createContext({} as IContextProps);

export const AddFocusSectorContextProvider = ({ children }) => {
  const [naceCodes, setNaceCodes] = React.useState([]);
  const [sectors, setSectors] = React.useState([]);

  // Get Sectors List
  const fetchSectors = async () => {
    const fetchData = await fetch(`${API_BASE_URL}sectors`);
    const jsonData = await fetchData.json();
    if (fetchData.status === 200) {
      console.log(jsonData['_embedded']['sectors']);
      // await Promise.all(jsonData['_embedded']['sectors'].map(async (item) => {
      //   const content =  await fetch(item['_links']['naceCode']['href']);
      //   const naceData = await content.json();
      //   console.log(naceData);
      // }));
      setSectors(jsonData['_embedded']['sectors']);
    }
  };

  // Get Nace Codes Dropdown list
  const getNaceCodes = async () => {
    const fetchData = await fetch(`${API_BASE_URL}naceCodes`);
    const jsonData = await fetchData.json();
    if (fetchData.status === 200) {
      setNaceCodes(
        jsonData['_embedded']['naceCodes'].map((c) => ({
          ...c,
          value: `${c.naceCodeCode} (${c.naceCodeDescr})`,
        })),
      );
      return jsonData['_embedded']['naceCodes'];
    }
  };

  // Add sector info
  const addFocusSector = async (body) => {
    const jsonBody = {
      grossValue: parseFloat(body.grossValue),
      naceCode: body.naceCode,
      ncompanies: parseFloat(body.ncompanies),
      nemployee: parseFloat(body.nemployee),
      nwasteGenerated: parseFloat(body.nwasteGenerated),
      nwasteIncinerated: parseFloat(body.nwasteIncinerated),
      nwasteLandfilled: parseFloat(body.nwasteLandfilled),
      nwasteRecycled: parseFloat(body.nwasteRecycled),
      region: body.region,
      sectorId: 0,
      sectorSource: body.sectorSource,
      turnover: parseFloat(body.turnover),
    };
    const response = await fetch(`${API_BASE_URL}sectors`, {
      method: 'POST',
      body: JSON.stringify(jsonBody),
      headers: { 'Content-Type': 'text/plain', 'Access-Control-Allow-Origin': '*' },
    });

    return response.status === 201;
  };

  React.useEffect(() => {
    getNaceCodes();
    fetchSectors();
  }, []);

  const defaultContext: any = {
    addFocusSector,
    naceCodes,
    sectors,
  };
  return (
    <AddFocusSectorContext.Provider value={defaultContext}>
      {children}
    </AddFocusSectorContext.Provider>
  );
};
