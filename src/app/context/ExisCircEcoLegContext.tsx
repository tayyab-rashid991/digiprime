import * as React from 'react';
import { API_BASE_URL } from 'app/config';

interface IContextProps {
  addEcoLegistlation:Function;
  existingCeLegislations:[];
}

export const ExisCircEcoLegContext = React.createContext({} as IContextProps);

export const ExisCircEcoLegContextProvider = ({children}) => {
  const [existingCeLegislations,setExistingCeLegislations] = React.useState([]);

  const getLegislations = async () => {
    const fetchData = await fetch(`${API_BASE_URL}/existingCeLegislations`);
    const jsonData = await fetchData.json();
    if(fetchData.status === 200){
      setExistingCeLegislations(jsonData['_embedded']['existingCeLegislations']);
    }
  };
  const addEcoLegislation = async (body) => {
    const response = await fetch(`https:localhost:8443/${API_BASE_URL}existingCeLegislations`, {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body:JSON.stringify(body),
    });
    if(response.status === 201){
      return true;
    }
    return false;

  };
  React.useEffect(() => {
    getLegislations();
  },              []);
  const defaultContext: any = {
    addEcoLegislation,
    existingCeLegislations,
  };
  return(
    <ExisCircEcoLegContext.Provider value={defaultContext}>
      {children}
    </ExisCircEcoLegContext.Provider>
  );
};
