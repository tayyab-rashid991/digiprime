import * as React from 'react';
import { API_BASE_URL } from 'app/config';

interface IContextProps {
  pChain: [];
  appDomain: [];
  addCompany: Function;
}

export const AddCompanyContext = React.createContext({} as IContextProps);

export const AddCompanyProvider = ({ children }) => {
  const [pChain, setPChain] = React.useState([]);
  const [appDomain, setAppDomain] = React.useState([]);

  const getPositionChain = async () => {
    const getPositionChainData = await fetch(`${API_BASE_URL}positionValueChains`, {
      method: 'GET',
    });
    if (getPositionChainData.status === 200) {
      const jsonData = await getPositionChainData.json();
      setPChain(jsonData['_embedded']['positionValueChains']);
    }
  };

  const getApplicationDomain = async () => {
    const getApplicationDomainData = await fetch(`${API_BASE_URL}applicationDomains`, {
      method: 'GET',
    });
    if (getApplicationDomainData.status === 200) {
      const jsonData = await getApplicationDomainData.json();
      setAppDomain(jsonData['_embedded']['applicationDomains']);
    }
  };
  const addCompany = async (body) => {
    const response = await fetch(`${API_BASE_URL}companies`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: { 'Content-Type': 'text/plain', 'Access-Control-Allow-Origin': '*' },
    });
    return response.status === 201;
  };
  React.useEffect(() => {
    getPositionChain();
    getApplicationDomain();
  }, []);
  const defaultContext: any = {
    pChain,
    appDomain,
    addCompany,
  };
  return <AddCompanyContext.Provider value={defaultContext}>{children}</AddCompanyContext.Provider>;
};
