import { createBrowserHistory } from 'history';

declare global {
  interface Window {
    dataLayer: any;
  }
}

const history = createBrowserHistory({
  basename: process.env.PUBLIC_URL
});

export default history;
